-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema recruitment
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema recruitment
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `recruitment` ;
USE `recruitment` ;

-- -----------------------------------------------------
-- Table `recruitment`.`faculty`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`faculty` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_ru` VARCHAR(500) NOT NULL,
  `name_eng` VARCHAR(500) NOT NULL,
  `total_places` INT NOT NULL,
  `budget_places` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name_ru` ASC) VISIBLE,
  INDEX `id` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_eng_UNIQUE` (`name_eng` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `full_name` VARCHAR(255) NOT NULL,
  `role` ENUM('admin', 'client') NOT NULL,
  `lang` VARCHAR(5) NOT NULL DEFAULT 'ru',
  `is_active` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) INVISIBLE,
  INDEX `id` (`id` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`enrollee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`enrollee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `city` VARCHAR(100) NOT NULL,
  `region` VARCHAR(255) NOT NULL,
  `educational_institution` VARCHAR(500) NOT NULL,
  `attachment` BLOB NULL,
  `is_blocked` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `id` (`id` ASC) VISIBLE,
  INDEX `enrollee_user_id_index` (`user_id` ASC) INVISIBLE,
  CONSTRAINT `enrollee_user_id_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `recruitment`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`subject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_ru` VARCHAR(255) NOT NULL,
  `name_eng` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name_ru` ASC) VISIBLE,
  INDEX `id` (`id` ASC) VISIBLE,
  UNIQUE INDEX `name_eng_UNIQUE` (`name_eng` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`grade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`grade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `enrollee_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  `value` INT NOT NULL,
  `exam_type` ENUM('diploma', 'entrance') NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `grade_enrollee_id_index` (`enrollee_id` ASC) INVISIBLE,
  INDEX `grade_subject_id_index` (`subject_id` ASC) INVISIBLE,
  INDEX `id` (`id` ASC) VISIBLE,
  CONSTRAINT `grade_enrollee_id_fk`
    FOREIGN KEY (`enrollee_id`)
    REFERENCES `recruitment`.`enrollee` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `grade_subject_id_fk`
    FOREIGN KEY (`subject_id`)
    REFERENCES `recruitment`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`faculty_enrollee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`faculty_enrollee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `faculty_id` INT NOT NULL,
  `enrollee_id` INT NOT NULL,
  INDEX `enrollee_id_index` (`enrollee_id` ASC) VISIBLE,
  INDEX `faculty_id_index` (`faculty_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `faculty_id_fk`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `recruitment`.`faculty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `enrollee_id_fk`
    FOREIGN KEY (`enrollee_id`)
    REFERENCES `recruitment`.`enrollee` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`faculty_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`faculty_subject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `faculty_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fs_fuculty_id_index` (`faculty_id` ASC) INVISIBLE,
  INDEX `fs_subject_id_index` (`subject_id` ASC) VISIBLE,
  CONSTRAINT `fs_faculty_id_fk`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `recruitment`.`faculty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fs_subject_id_fk`
    FOREIGN KEY (`subject_id`)
    REFERENCES `recruitment`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`faculties_sheet_report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`faculties_sheet_report` (
  `faculty_id` INT NULL,
  `full_name` INT NULL,
  `email` INT NULL,
  `is_blocked` INT NULL,
  `diploma_sum` INT NULL,
  `entrance_sum` INT NULL,
  `total_sum` INT NULL,
  INDEX `fsr_fuculty_id_fk_idx` (`faculty_id` ASC) VISIBLE,
  CONSTRAINT `fsr_fuculty_id_fk`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `recruitment`.`faculty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `recruitment`.`enrollee_grades_sum`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `recruitment`.`enrollee_grades_sum` (
  `faculty_id` INT NULL,
  `enrollee_id` INT NULL,
  `diploma_sum` INT NULL,
  `entrance_sum` VARCHAR(45) NULL,
  INDEX `egs_faculty_id_idx` (`faculty_id` ASC) VISIBLE,
  INDEX `egs_enrollee_id_idx` (`enrollee_id` ASC) VISIBLE,
  CONSTRAINT `egs_faculty_id`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `recruitment`.`faculty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `egs_enrollee_id`
    FOREIGN KEY (`enrollee_id`)
    REFERENCES `recruitment`.`enrollee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- View `recruitment`.`enrollee_grades_sum`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `recruitment`.`enrollee_grades_sum` ;
DROP TABLE IF EXISTS `recruitment`.`enrollee_grades_sum`;
USE `recruitment`;
CREATE OR REPLACE VIEW `enrollee_grades_sum` AS
SELECT
    fe.faculty_id,
    g.enrollee_id,
    SUM(CASE `exam_type`
            WHEN 'entrance' THEN g.value
            ELSE 0
        END) AS `entrance_sum`,
    SUM(CASE `exam_type`
            WHEN 'diploma' THEN g.value
            ELSE 0
        END) AS `diploma_sum`
FROM faculty_enrollee fe
         INNER JOIN
     grade g ON fe.enrollee_id = g.enrollee_id
GROUP BY fe.faculty_id, fe.enrollee_id;

-- -----------------------------------------------------
-- View `recruitment`.`faculties_sheet_report`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `recruitment`.`faculties_sheet_report` ;
DROP TABLE IF EXISTS `recruitment`.`faculties_sheet_report`;
USE `recruitment`;
CREATE OR REPLACE VIEW faculties_sheet_report AS
SELECT
    egs.faculty_id,
    u.full_name,
    u.email,
    e.is_blocked,
    entrance_sum,
    diploma_sum,
    entrance_sum + diploma_sum AS total_sum
FROM
    enrollee_grades_sum egs
        INNER JOIN
    faculty f ON egs.enrollee_id = f.id
        INNER JOIN
    enrollee e ON egs.enrollee_id = e.id
        INNER JOIN
    user u ON e.user_id = u.id
ORDER BY is_blocked ASC , `total_sum` DESC;
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
