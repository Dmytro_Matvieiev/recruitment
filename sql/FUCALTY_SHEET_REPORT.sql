SELECT
    faculty_id,
    en.full_name,
    en.email,
    e.is_blocked,
    preliminary_sum,
    diploma_sum,
    preliminary_sum + diploma_sum AS total_sum
FROM
    (SELECT
         fe.faculty_id AS faculty_id,
         g.enrollee_id AS enrollee_id,
         SUM(CASE exam_type
                 WHEN 'entrance' THEN g.value
                 ELSE 0
             END) AS `preliminary_sum`,
         SUM(CASE `exam_type`
                 WHEN 'diploma' THEN g.value
                 ELSE 0
             END) AS `diploma_sum`
     FROM
         faculty_enrollee fe
             INNER JOIN grade g ON fe.enrollee_id = g.enrollee_id
     GROUP BY g.enrollee_id) enrollee_grades_sum
        INNER JOIN faculty f ON enrollee_grades_sum.faculty_id = f.id
        INNER JOIN enrollee e ON enrollee_id = e.id
        INNER JOIN (
        select u.*, e.id as enrolleeId from user u inner join enrollee e on u.id = e.user_id
    ) en ON en.enrolleeId = e.id
WHERE
        faculty_id = ?
ORDER BY is_blocked ASC , `total_sum` DESC;