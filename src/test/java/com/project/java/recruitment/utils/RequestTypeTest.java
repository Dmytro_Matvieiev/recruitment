package com.project.java.recruitment.utils;

import org.junit.Test;

public class RequestTypeTest {

    @Test
    public void shouldGetRequestTypePost() {
        RequestType.values();
        RequestType.valueOf(RequestType.POST.name());
    }

    @Test
    public void shouldGetRequestTypeGet() {
        RequestType.values();
        RequestType.valueOf(RequestType.GET.name());
    }
}
