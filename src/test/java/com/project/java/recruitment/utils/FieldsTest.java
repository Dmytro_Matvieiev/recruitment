package com.project.java.recruitment.utils;

import com.project.java.recruitment.utils.Fields;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;


public class FieldsTest {
    @Test
    public void testConstructorIsPrivate() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<Fields> constructor = Fields.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }
}
