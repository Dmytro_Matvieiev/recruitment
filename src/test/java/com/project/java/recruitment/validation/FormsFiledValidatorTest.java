package com.project.java.recruitment.validation;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.*;

public class FormsFiledValidatorTest {
    @Test
    public void testConstructorIsPrivate()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<FormsFiledValidator> constructor = FormsFiledValidator.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldCheckIsFilledParameter() {
        assertTrue(FormsFiledValidator.isFilled("fullName", "email"));
        assertFalse(FormsFiledValidator.isFilled("fullName", ""));
        assertFalse(FormsFiledValidator.isFilled(null, null));
        assertFalse(FormsFiledValidator.isFilled(null));
    }

    @Test
    public void shouldBeLatinWord() {
        assertEquals(true, FormsFiledValidator.isLatinWord("university", "recruitment"));
        assertFalse(FormsFiledValidator.isLatinWord("university1", "приемная"));
        assertFalse(FormsFiledValidator.isLatinWord(null, null));
    }

    @Test
    public void shouldBeCyrillicWord() {
        assertEquals(true, FormsFiledValidator.isCyrillicWord("приемная", "комиссия"));
        assertFalse(FormsFiledValidator.isCyrillicWord("university", "recruitment1"));
        assertFalse(FormsFiledValidator.isCyrillicWord(null, null));
    }

    @Test
    public void shouldBePositiveDecimalNumber() {
        assertTrue(FormsFiledValidator.isPositiveDecimalNumber("1"));
        assertFalse(FormsFiledValidator.isPositiveDecimalNumber("-11"));
        assertFalse(FormsFiledValidator.isPositiveDecimalNumber(null, null));
    }

    @Test
    public void shouldBePositiveNumber() {
        assertTrue(FormsFiledValidator.isPositive(1.2));
        assertFalse(FormsFiledValidator.isPositive(-1));
        assertFalse(FormsFiledValidator.isPositive(null, null));
    }

    @Test
    public void shouldBudgetLowerTotal() {
        assertTrue(FormsFiledValidator.checkBudgetLowerTotal(400, 450));
        assertFalse(FormsFiledValidator.checkBudgetLowerTotal(450, 450));
    }
}
