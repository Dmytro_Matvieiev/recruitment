package com.project.java.recruitment.validation;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SubjectInputValidatorTest {
    @Test
    public void testConstructorIsPrivate()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<SubjectInputValidator> constructor = SubjectInputValidator.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldValidateSubject() {
        assertTrue(SubjectInputValidator.validateParameters("Информатика", "Informatics"));
    }

    @Test
    public void shouldNotValidateSubject() {
        assertFalse(SubjectInputValidator.validateParameters("Информатика", "Информатика"));
        assertFalse(SubjectInputValidator.validateParameters("", ""));
    }
}
