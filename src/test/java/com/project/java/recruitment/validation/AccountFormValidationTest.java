package com.project.java.recruitment.validation;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AccountFormValidationTest {

    @Test
    public void testConstructorIsPrivate()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<AccountFormValidation> constructor = AccountFormValidation.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    private static final String FULL_NAME = "New User";
    private static final String CITY = "Dnipro";


    @Test
    public void shouldValidateUserParameters() {
        assertTrue(AccountFormValidation.validateUserParameters(FULL_NAME, "ivan@gmail.com", "1", "ru"));
    }

    @Test
    public void shouldNotValidateUserParametersIfEmailNotExistAmp() {
        assertFalse(AccountFormValidation.validateUserParameters(FULL_NAME, "ivangmail.com", "1", "ru"));
    }

    @Test
    public void shouldNotValidateUserParametersIfSomeNotFilled() {
        assertFalse(AccountFormValidation.validateUserParameters(FULL_NAME, "", "1", "ru"));
    }

    @Test
    public void shouldValidateEnrolleeParameters() {
        assertTrue(AccountFormValidation.validateEnrolleeParameters(CITY, CITY, "school 31"));
    }

    @Test
    public void shouldNotValidateEnrolleeParametersIfEducationalIsEmptyOrOtherField() {
        assertFalse(AccountFormValidation.validateEnrolleeParameters(CITY, CITY, ""));
        assertFalse(AccountFormValidation.validateEnrolleeParameters(CITY, "", "31"));
        assertFalse(AccountFormValidation.validateEnrolleeParameters("", CITY, "31"));
    }
}
