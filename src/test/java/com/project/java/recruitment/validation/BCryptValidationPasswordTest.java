package com.project.java.recruitment.validation;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.*;

public class BCryptValidationPasswordTest {
    private String password = "1234";

    @Test
    public void testConstructorIsPrivate()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<BCryptValidationPassword> constructor = BCryptValidationPassword.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldCreateEncodePassword() {
        assertNotNull(BCryptValidationPassword.getEncodePassword(password));
    }

    @Test
    public void shouldCompareEncodeAndDecodePassword() {
        assertTrue(BCryptValidationPassword.isValidPassword(
                password, BCryptValidationPassword.getEncodePassword(password)));
    }
}
