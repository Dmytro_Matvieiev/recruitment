package com.project.java.recruitment.validation;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FacultyInputValidatorTest {

    @Test
    public void testConstructorIsPrivate() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<FacultyInputValidator> constructor = FacultyInputValidator.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldValidateParameters() {
        assertTrue(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "400", "500"));
    }

    @Test
    public void shouldNotValidateIfBudgetPlacesMoreThanTotalPlaces() {
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "600", "500"));
    }

    @Test
    public void shouldNotValidateIfBudgetPlacesOrTotalPlacesNotPositive() {
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "-600", "500"));
    }

    @Test
    public void shouldNotValidateIfBudgetPlacesOrTotalPlacesNotPositiveDecimal() {
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "-400.5", ""));
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "", ""));
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "New faculty", "", "-400.5"));
    }

    @Test
    public void shouldNotValidateIfFacultyNameRuNotCyrillicOrFacultyNameEngNotLatin() {
        assertFalse(FacultyInputValidator.validateParameters(
                "New faculty", "New faculty", "400", "500"));
        assertFalse(FacultyInputValidator.validateParameters(
                "Новый факультет", "Новый факультет", "400", "500"));
    }
}
