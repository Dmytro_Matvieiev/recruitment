package com.project.java.recruitment.dao.datasourse;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import org.junit.Test;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DataSourceFactoryTest {

    @Test
    public void shouldGetDataSource() {
        DataSource dataSource = DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI);
        DataSource dataSource2 = DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI);

        assertTrue(dataSource.getClass().equals(MysqlConnectionPoolDataSource.class));
        assertEquals(dataSource2.getClass(), MysqlConnectionPoolDataSource.class);
    }
}
