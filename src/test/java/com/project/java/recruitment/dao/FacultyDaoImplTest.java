package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultyDaoImpl;
import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Faculty;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class FacultyDaoImplTest {
    private Faculty faculty;
    private static FacultyDao facultyDao;
    private static int facultyId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        facultyDao = new FacultyDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Faculty facultyToDelete = new Faculty(); // clean up db
        facultyToDelete.setId(facultyId);
        facultyDao.deleteFaculty(facultyToDelete.getId());
    }

    @Before
    public void setUp() throws Exception {
        faculty = new Faculty();

        faculty.setNameRu("Новый факультет ru");
        faculty.setNameEng("New faculty eng");
        faculty.setTotalPlaces(340);
        faculty.setBudgetPlaces(300);

        facultyDao.insertFaculty(faculty);
        facultyId = faculty.getId();
    }

    @After
    public void tearDown() throws Exception {
        facultyDao.deleteFaculty(faculty.getId());
        faculty = null;
    }

    @Test
    public void shouldCallFacultyDefaultConstructor() {
        new FacultyDaoImpl(null);
    }

    @Test
    public void shouldCreateNewFaculty() {
        facultyDao.deleteFaculty(faculty.getId());
        faculty.setId(-1);// error code
        facultyDao.insertFaculty(faculty);
        assertThat(faculty.getId(), not(equalTo(-1)));
    }

    @Test
    public void shouldUpdateFaculty() {
        faculty.setNameEng("updated new name eng");
        facultyDao.updateFaculty(faculty);
        assertThat(faculty.getNameEng(), equalTo(facultyDao.findFacultyById(facultyId).getNameEng()));
    }

    @Test
    public void shouldDeleteFaculty() {
        facultyDao.deleteFaculty(faculty.getId());
        assertThat(facultyDao.findFacultyById(facultyId), is(equalTo(null)));
    }

    @Test
    public void shouldFindFacultyById() {
        assertNotNull(facultyDao.findFacultyById(facultyId));
    }

    @Test
    public void shouldFindFacultyByNameRuOrEng() {
        assertNotNull(facultyDao.findFacultyByName(faculty.getNameRu()));
        assertNotNull(facultyDao.findFacultyByName(faculty.getNameEng()));
    }

    @Test
    public void shouldFindAllFaculties() {
        List<Faculty> faculties = facultyDao.findAllFaculty();
        assertThat(faculties.size(), is(not(0)));
    }
}
