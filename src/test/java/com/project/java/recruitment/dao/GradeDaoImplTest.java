package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.*;
import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Grade;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.domain.User;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class GradeDaoImplTest {
    private static UserDao userDao;
    private static EnrolleeDao enrolleeDao;
    private static SubjectDao subjectDao;

    private static Enrollee enrollee;
    private static User user;
    private static Subject subject;

    private static GradeDaoImpl gradeDao;
    private Grade grade;
    private static int gradeId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        userDao = new UserDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        user = new User();
        user.setFullName("Cristiano Ronaldo");
        user.setEmail("cr7@gmail.com");
        user.setPassword("1234");
        user.setRole("client");
        user.setLang("ru");

        userDao.insertUser(user);

        enrolleeDao = new EnrolleeDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        enrollee = new Enrollee();
        enrollee.setCity("Washington");
        enrollee.setRegion("DC");
        enrollee.setEducationalInstitution("School 31");
        enrollee.setBlocked(false);
        enrollee.setUserId(user.getId());

        enrolleeDao.insertEnrollee(enrollee);

        subjectDao = new SubjectDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        subject = new Subject();
        subject.setNameRu("новый предмет ru");
        subject.setNameEng("new subject eng");

        subjectDao.insertSubject(subject);

        gradeDao = new GradeDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Grade gradeToDelete = new Grade();// clean up db
        gradeToDelete.setId(gradeId);
        gradeDao.deleteGrade(gradeToDelete.getSubjectId());

        enrolleeDao.deleteEnrollee(enrollee.getId());
        userDao.deleteUser(user.getId());
        subjectDao.deleteSubject(subject.getId());
    }

    @Before
    public void setUp() throws Exception {
        grade = new Grade();
        grade.setEnrolleeId(enrollee.getId());
        grade.setSubjectId(subject.getId());
        grade.setValue(12);
        grade.setExamType("diploma");

        gradeDao.insertGrade(grade);
        gradeId = grade.getId();
    }

    @After
    public void tearDown() throws Exception {
        gradeDao.deleteGrade(grade.getId());
        grade = null;
    }

    @Test
    public void shouldCallGradeDaoDefaultConstructor() {
        assertNotNull(new GradeDaoImpl(null));
    }

    @Test
    public void shouldCreateNewGrade() {
        gradeDao.deleteGrade(grade.getId());
        grade.setId(-1);
        gradeDao.insertGrade(grade);
        assertThat(grade.getId(), not(equalTo(-1)));
        gradeDao.deleteGrade(grade.getId());
    }

    @Test
    public void shouldUpdateGrade() {
        grade.setValue(10);
        gradeDao.updateGrade(grade);
        assertThat(grade.getValue(), equalTo(gradeDao.find(gradeId).getValue()));
    }

    @Test
    public void shouldDeleteGrade() {
        gradeDao.deleteGrade(grade.getId());
        assertThat(gradeDao.find(gradeId), is(equalTo(null)));
    }

    @Test
    public void shouldFindGradeById() {
        assertNotNull(gradeDao.find(gradeId));
    }

    @Test
    public void shouldFindAllGrades() {
        List<Grade> grades = gradeDao.findAll();
        assertThat(grades.isEmpty(), is(equalTo(false)));
    }
}
