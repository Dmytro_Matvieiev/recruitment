package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.EnrolleeSheetReport;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class SheetReportDaoImplTest {
    private static SheetReportDao sheetReportDao;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        sheetReportDao = new SheetReportDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        sheetReportDao = null;
    }

    @Test
    public void shouldCallReportSheetDaoDefaultConstructor() {
        assertNotNull(new SheetReportDaoImpl(null));
    }

    @Test
    public void shouldGetSheetReport() {
        List<EnrolleeSheetReport> report = sheetReportDao.getReport(1);
        assertThat(report.isEmpty(), is(anyOf(is(Boolean.TRUE), is(Boolean.FALSE))));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotCreateSheetReport() {
        sheetReportDao.create(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotUpdateSheetReport() {
        sheetReportDao.update(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotDeleteSheetReport() {
        sheetReportDao.delete(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotFindSheetReport() {
        sheetReportDao.find(0);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotFindAllSheetReport() {
        sheetReportDao.findAll();
    }
}
