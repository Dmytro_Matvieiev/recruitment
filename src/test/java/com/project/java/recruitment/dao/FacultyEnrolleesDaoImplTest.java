package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultyEnrollee;
import com.project.java.recruitment.domain.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FacultyEnrolleesDaoImplTest {
    private static UserDao userDao;
    private static EnrolleeDao enrolleeDao;
    private static FacultyDao facultyDao;

    private static FacultyEnrolleesDao facultyEnrolleesDao;

    private static Faculty faculty;
    private static Enrollee enrollee;
    private static User user;

    private FacultyEnrollee facultyEnrollee;
    private static int facultyEnrolleesId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        userDao = new UserDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        user = new User();
        user.setFullName("LeBron James");
        user.setEmail("lebron23james@gmail.com");
        user.setPassword("1234");
        user.setRole("client");
        user.setLang("ru");

        userDao.insertUser(user);

        enrolleeDao = new EnrolleeDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        enrollee = new Enrollee();
        enrollee.setCity("Los Angeles");
        enrollee.setRegion("LA");
        enrollee.setEducationalInstitution("School 31");
        enrollee.setBlocked(false);
        enrollee.setUserId(user.getId());

        enrolleeDao.insertEnrollee(enrollee);

        facultyDao = new FacultyDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        faculty = new Faculty();

        faculty.setNameRu("new ru");
        faculty.setNameEng("name eng");
        faculty.setTotalPlaces(400);
        faculty.setBudgetPlaces(380);

        facultyDao.insertFaculty(faculty);

        facultyEnrolleesDao = new FacultyEnrolleesDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        FacultyEnrollee facultyEnrolleesToDelete = new FacultyEnrollee();// clean up db

        facultyEnrolleesToDelete.setId(facultyEnrolleesId);
        facultyEnrolleesDao.deleteFacultyEnrollee(facultyEnrolleesToDelete);

        enrolleeDao.deleteEnrollee(enrollee.getId());
        userDao.deleteUser(user.getId());
        facultyDao.deleteFaculty(faculty.getId());
    }

    @Before
    public void setUp() throws Exception {
        facultyEnrollee = new FacultyEnrollee(faculty, enrollee);
        facultyEnrolleesDao.insertFacultyEnrollee(facultyEnrollee);
        facultyEnrolleesId = facultyEnrollee.getId();
    }

    @After
    public void tearDown() throws Exception {
        facultyEnrolleesDao.deleteFacultyEnrollee(facultyEnrollee);
        facultyEnrollee = null;
    }

    @Test
    public void shouldCallFacultyEnrolleesDaoDefaultConstructor() {
        assertNotNull(new FacultyEnrolleesDaoImpl(null));
    }

    @Test
    public void shouldCreateNewFacultyEnrollee() {
        facultyEnrolleesDao.deleteFacultyEnrollee(facultyEnrollee);
        facultyEnrollee.setId(-1);// error code
        facultyEnrolleesDao.insertFacultyEnrollee(facultyEnrollee);
        assertThat(facultyEnrollee.getId(), not(equalTo(-1)));
        facultyEnrolleesDao.deleteFacultyEnrollee(facultyEnrollee);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotUpdateFacultyEnrollee() {
        facultyEnrolleesDao.update(facultyEnrollee);
    }

    @Test
    public void shouldDeleteacultyEnrollee() {
        facultyEnrolleesDao.deleteFacultyEnrollee(facultyEnrollee);
        assertThat(facultyEnrolleesDao.findById(facultyEnrolleesId), is(equalTo(null)));
    }

    @Test
    public void shouldFindFacultyEnrolleeById() {
        assertNotNull(facultyEnrolleesDao.findById(facultyEnrolleesId));
    }

    @Test
    public void shouldFindFacultyEnrolleeByEnrolleeIdAndFacultyId() {
        assertNotNull(facultyEnrolleesDao.find(facultyEnrollee));
    }

    @Test
    public void shouldFindAllFacultyEnrollee() {
        List<FacultyEnrollee> facultyEnrolleesList = facultyEnrolleesDao.findAll();
        assertTrue(facultyEnrolleesList.size() > 0);
    }
}
