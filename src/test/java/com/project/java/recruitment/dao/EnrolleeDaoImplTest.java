package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.User;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class EnrolleeDaoImplTest {
    private static EnrolleeDao enrolleeDao;
    private static UserDao userDao;
    private static User user;
    private Enrollee enrollee;
    private static int enrolleeId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        userDao = new UserDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        user = new User();
        user.setFullName("Michael Jordan");
        user.setEmail("jumpman@gmail.com");
        user.setPassword("1234");
        user.setRole("client");
        user.setLang("ru");

        userDao.updateUser(user);

        enrolleeDao = new EnrolleeDaoImpl(
                DataSourceFactory
                        .getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Enrollee enrolleeToDelete = new Enrollee(); // clean up db
        enrolleeToDelete.setId(enrolleeId);
        enrolleeDao.deleteEnrollee(enrolleeToDelete.getId());
        userDao.deleteUser(user.getId());
    }

    @Before
    public void setUp() throws Exception {
        enrollee = new Enrollee();
        enrollee.setCity("Chicago");
        enrollee.setRegion("Illinois");
        enrollee.setEducationalInstitution("High School 31");
        enrollee.setBlocked(false);
        enrollee.setUserId(user.getId());

        enrolleeDao.insertEnrollee(enrollee);
        enrolleeId = enrollee.getId();
    }

    @After
    public void tearDown() throws Exception {
        enrolleeDao.deleteEnrollee(enrollee.getId());
        enrollee = null;
    }

    @Test
    public void shouldCallEnrolleeDaoDefaultConstructor() {
        assertNotNull(new EnrolleeDaoImpl(null));
    }

    @Test
    public void shouldCreateNewEnrollee() {
        enrolleeDao.deleteEnrollee(enrollee.getId());

        enrollee.setId(-1);// error code
        enrolleeDao.insertEnrollee(enrollee);

        assertThat(enrollee.getId(), not(equalTo(-1)));

        enrolleeDao.deleteEnrollee(enrollee.getId());
    }

    @Test
    public void shouldUpdateEnrollee() {
        enrollee.setBlocked(true);
        enrolleeDao.updateEnrollee(enrollee);

        assertThat(enrollee.isBlocked(),
                equalTo(enrolleeDao.findEnrollee(enrolleeId).isBlocked()));
    }

    @Test
    public void shouldDeleteEnrollee() {
        enrolleeDao.deleteEnrollee(enrollee.getId());
        assertThat(enrolleeDao.findEnrollee(enrolleeId), is(equalTo(null)));
    }

    @Test
    public void shouldFindEnrolleeById() {
        assertNotNull(enrolleeDao.findEnrollee(enrolleeId));
    }

    @Test
    public void sholdFindEnrolleeByUser() {
        assertNotNull(enrolleeDao.findEnrollee(user));
    }

    @Test
    public void shouldFindAllEnrollees() {
        List<Enrollee> users = enrolleeDao.findAllEnrollee();
        assertThat(users.isEmpty(), is(false));
    }

    @Test
    public void shouldFindAllFacultyEnrollees() {
        Faculty faculty = new Faculty();
        faculty.setId(100_000_000); // no faculty with such id
        List<Enrollee> facultyEnrollees = enrolleeDao.findAllFacultyEnrollee(faculty);
        assertThat(facultyEnrollees.size(), equalTo(0));
    }
}
