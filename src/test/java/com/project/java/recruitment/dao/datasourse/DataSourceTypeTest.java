package com.project.java.recruitment.dao.datasourse;

import com.project.java.recruitment.dao.datasource.DataSourceType;
import org.junit.Test;

public class DataSourceTypeTest {

    @Test
    public void shouldGetTypeDatasourceWithoutJNDI() {
        DataSourceType.values();
        DataSourceType.valueOf(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI.name());
    }

    @Test
    public void shouldGetTypeDatasourceWithJNDI() {
        DataSourceType.values();
        DataSourceType.valueOf(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI.name());
    }
}
