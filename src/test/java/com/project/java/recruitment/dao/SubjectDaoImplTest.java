package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.SubjectDaoImpl;
import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.Subject;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class SubjectDaoImplTest {
    private Subject subject;
    private static SubjectDao subjectDao;
    private static int subjectId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        subjectDao = new SubjectDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Subject subjectToDelete = new Subject(); // clean up db
        subjectToDelete.setId(subjectId);
        subjectDao.deleteSubject(subjectToDelete.getId());
    }

    @Before
    public void setUp() throws Exception {
        subject = new Subject();
        subject.setNameRu("subject ru");
        subject.setNameEng("name eng");
        subjectDao.insertSubject(subject);
        subjectId = subject.getId();
    }

    @After
    public void tearDown() throws Exception {
        subjectDao.deleteSubject(subject.getId());
        subject = null;
    }

    @Test
    public void shouldCallSubjectDaoDefaultConstructor() {
        assertNotNull(new SubjectDaoImpl(null));
    }

    @Test
    public void shouldCreateNewSubject() {
        subjectDao.deleteSubject(subject.getId());
        subject.setId(-1);// error code
        subjectDao.insertSubject(subject);

        assertThat(subject.getId(), not(equalTo(-1)));

        subjectDao.deleteSubject(subject.getId());
    }

    @Test
    public void shouldUpdateSubject() {
        subject.setNameEng("new subject name");
        subjectDao.updateSubject(subject);
        assertThat(subject.getNameEng(), equalTo(subjectDao.findSubject(subjectId).getNameEng()));
    }

    @Test
    public void shouldDeleteSubject() {
        subjectDao.deleteSubject(subject.getId());
        assertThat(subjectDao.findSubject(subjectId), is(equalTo(null)));
    }

    @Test
    public void shouldFindSubjectById() {
        assertNotNull(subjectDao.findSubject(subjectId));
    }

    @Test
    public void shouldFindSubjectByNameRuOrEng() {
        assertNotNull(subjectDao.findSubjectByName(subject.getNameEng()));
        assertNotNull(subjectDao.findSubjectByName(subject.getNameRu()));
    }

    @Test
    public void shouldFindAllSubject() {
        List<Subject> subjects = subjectDao.findAllSubjects();
        assertFalse(subjects.isEmpty());
    }

    @Test
    public void shouldFindAllFacultySubjects() {
        Faculty faculty = new Faculty();
        faculty.setId(100_000_000);// so faculty with such id
        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(faculty);
        assertEquals(0, facultySubjects.size());
    }

    @Test
    public void shouldFindAllNotFacultySubjects() {
        Faculty faculty = new Faculty();
        faculty.setId(100_000_000);// so faculty with such id
        List<Subject> notFacultySubjects = subjectDao.findAllNotFacultySubjects(faculty);
        List<Subject> allSubjects = subjectDao.findAllSubjects();
        assertThat(notFacultySubjects.size(), is(equalTo(allSubjects.size())));
    }
}
