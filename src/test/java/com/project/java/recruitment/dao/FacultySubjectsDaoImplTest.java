package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultySubject;
import com.project.java.recruitment.domain.Subject;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class FacultySubjectsDaoImplTest {
    private static FacultyDao facultyDao;
    private static Faculty faculty;
    private static SubjectDao subjectDao;
    private static Subject subject;

    private static FacultySubjectsDao facultySubjectsDao;
    private FacultySubject facultySubject;
    private static int facultySubjectsId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        facultyDao = new FacultyDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        faculty = new Faculty();

        faculty.setNameRu("новый факлуьтет ru");
        faculty.setNameEng("new faculty eng");
        faculty.setTotalPlaces(420);
        faculty.setBudgetPlaces(330);

        facultyDao.insertFaculty(faculty);

        subjectDao = new SubjectDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

        subject = new Subject();
        subject.setNameRu("новый пердмет ru");
        subject.setNameEng("new subject eng");

        subjectDao.insertSubject(subject);

        facultySubjectsDao = new FacultySubjectsDaoImpl(
                DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        FacultySubject facultySubjectsToDelete = new FacultySubject(); // clean up db
        facultySubjectsToDelete.setId(facultySubjectsId);
        facultySubjectsDao.deleteFacultySubjects(facultySubjectsToDelete);

        facultyDao.deleteFaculty(faculty.getId());
        subjectDao.deleteSubject(subject.getId());
    }

    @Before
    public void setUp() throws Exception {
        facultySubject = new FacultySubject(subject.getId(), faculty.getId());
        facultySubjectsDao.insertFacultySubjects(facultySubject);
        facultySubjectsId = facultySubject.getId();
    }

    @After
    public void tearDown() throws Exception {
        facultySubjectsDao.deleteFacultySubjects(facultySubject);
        facultySubject = null;
    }

    @Test
    public void shouldCallFacultySubjectsDaoDefaultConstructor() {
        new FacultySubjectsDaoImpl(null);
    }

    @Test
    public void shouldCreateFacultySubjects() {
        facultySubjectsDao.deleteFacultySubjects(facultySubject);
        facultySubject.setId(-1);// error code
        facultySubjectsDao.insertFacultySubjects(facultySubject);
        assertThat(facultySubject.getId(), not(equalTo(-1)));
        facultySubjectsDao.deleteFacultySubjects(facultySubject);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldNotUpdateFacultySubjects() {
        facultySubjectsDao.updateFacultySubjects(facultySubject);
    }

    @Test
    public void shouldDeleteFacultySubjects() {
        facultySubjectsDao.deleteFacultySubjects(facultySubject);
        assertThat(facultySubjectsDao.find(facultySubjectsId), is(equalTo(null)));
    }

    @Test
    public void shouldDeleteAllSubjectsByFaculty() {
        facultySubjectsDao.deleteAllFacultySubjects(faculty);
        assertThat(facultySubjectsDao.find(facultySubjectsId), is(nullValue()));
    }

    @Test
    public void shouldFindFacultySubjectsById() {
        assertNotNull(facultySubjectsDao.find(facultySubjectsId));
    }

    @Test
    public void shouldFindAllFacultySubjects() {
        List<FacultySubject> facultySubjectsList = facultySubjectsDao.findAll();
        assertTrue(facultySubjectsList.size() > 0);
    }
}
