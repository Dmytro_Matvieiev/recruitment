package com.project.java.recruitment.dao;

import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;
import com.project.java.recruitment.domain.User;
import org.junit.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class UserDaoImplTest {
    private User user;
    private static UserDao userDao;
    private static int userId;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        userDao = new UserDaoImpl(DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITHOUT_JNDI)) {
            @Override
            public Connection getConnection() throws SQLException {
                Connection connection = dataSource.getConnection();
                connection.setAutoCommit(false);
                return connection;
            }
        };

    }

    @Before
    public void setUp() throws Exception {
        user = new User();

        user.setFullName("Thiago Silva");
        user.setEmail("thiagosilva@gmail.com");
        user.setPassword("1234");
        user.setRole("client");
        user.setLang("ru");

        userDao.insertUser(user);
        userId = user.getId();
    }

    @After
    public void tearDown() throws Exception {
        userDao.deleteUser(user.getId());
        user = null;
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        User userToDelete = new User();
        userToDelete.setId(userId);
        userDao.deleteUser(userToDelete.getId()); // clean up db
    }

    @Test
    public void shouldCallUserDaoDefaultConstructor() {
        assertNotNull(new UserDaoImpl(null));
    }

    @Test
    public void shouldCreateNewUser() {
        userDao.deleteUser(user.getId());

        user.setId(-1);// error code
        userDao.insertUser(user);

        assertThat(user.getId(), not(equalTo(-1)));

        userDao.deleteUser(user.getId());
    }

    @Test
    public void shouldUpdateUser() {
        user.setEmail("silva.thiago@gmail.com");
        userDao.updateUser(user);
        assertThat(user.getEmail(), equalTo(userDao.find(userId).getEmail()));
    }

    @Test
    public void shouldDeleteUserById() {
        userDao.deleteUser(user.getId());
        assertThat(userDao.find(userId), is(equalTo(null)));
    }

    @Test
    public void shouldFindUserById() {
        assertNotNull(userDao.find(userId));
    }

    @Test
    public void shouldFindUserByEmailAndPassword() {
        assertNotNull(userDao.find(user.getEmail(), user.getPassword()));
    }

    @Test
    public void shouldFindUserByEmail() {
        assertNotNull(userDao.find(user.getEmail()));
    }

    @Test
    public void shouldFindAllUsers() {
        List<User> users = userDao.findAllUsers();
        assertThat(users.isEmpty(), is(false));
    }
}
