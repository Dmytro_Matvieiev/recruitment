package com.project.java.recruitment.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User user;

    @Before
    public void setUp() {
        user = new User();

        user.setFullName("Thiago Silva");
        user.setEmail("thiagosilva@gmail.com");
        user.setPassword("1234");
        user.setRole("client");
        user.setLang("ru");
    }

    @Test
    public void shouldCallNotDefaultConstructor() {
        assertNotNull( user = new User
                ("thiagosilva@gmail.com", "1234", "Thiago Silva", Role.CLIENT,
                        "ru", true));
    }

    @Test
    public void shouldCallToStringUser() {
        String expected =
                "User{ email='thiagosilva@gmail.com', password='1234', " +
                        "fullName='Thiago Silva', role='client', lang='ru', isActive=false}";

        assertEquals(expected, user.toString());
    }

    @Test
    public void shouldEqualsToUsers() {
        User user2 = new User();
        user2.setFullName("Thiago Silva");
        user2.setEmail("thiagosilva@gmail.com");
        user2.setPassword("1234");
        user2.setRole("admin"); // different
        user2.setLang("eng"); // different

        assertTrue(user.equals(user2));
    }

    @Test
    public void shouldSetActiveStatus() {
        boolean activeStatus = user.isActive();
        user.setActive(true);
        assertNotEquals(user.isActive(), activeStatus);
    }

    @Test
    public void testEqualsHashCode() {
        User user2 = new User();
        user2.setFullName("Thiago Silva");
        user2.setEmail("thiagosilva@gmail.com");
        user2.setPassword("1234");
        user2.setRole("admin"); // different
        user2.setLang("eng"); // different
        assertTrue(user.equals(user2) && user2.equals(user));
        assertTrue(user.hashCode() == user2.hashCode());
    }
}
