package com.project.java.recruitment.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubjectTest {

    private Subject subject;

    @Before
    public void setUp() {
        subject = new Subject();

        subject.setNameRu("Украинский язык");
        subject.setNameEng("Ukrainian");
    }

    @Test
    public void shouldCallNotDefaultConstructor() {
        assertNotNull( subject = new Subject("Украинский язык", "Ukrainian"));
    }

    @Test
    public void shouldCallToStringUser() {
        String expected =
                "Subject{nameRu='Украинский язык', nameEng='Ukrainian'}";

        assertEquals(expected, subject.toString());
    }

    @Test
    public void shouldEqualsToUsers() {
        Subject subject1 = new Subject();
        subject1.setNameRu("Украинский язык");
        subject1.setNameEng("Ukrainian");

        assertTrue(subject.equals(subject1));
    }

    @Test
    public void testEqualsHashCode() {
        Subject subject1 = new Subject();
        subject1.setNameRu("Украинский язык");
        subject1.setNameEng("Ukrainian");
        assertTrue(subject.equals(subject1) && subject1.equals(subject));
        assertTrue(subject.hashCode() == subject1.hashCode());
    }
}
