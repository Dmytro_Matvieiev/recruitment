package com.project.java.recruitment.domain;

import com.project.java.recruitment.dao.datasource.DataSourceType;
import org.junit.Test;

public class RoleTest {

    @Test
    public void shouldGetRoleClient() {
        Role.values();
        Role.CLIENT.getName();
    }

    @Test
    public void shouldGetRoleAdmin() {
        Role.values();
        Role.ADMIN.getName();
    }
}
