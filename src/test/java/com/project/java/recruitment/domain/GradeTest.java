package com.project.java.recruitment.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GradeTest {
    private Grade grade;

    @Before
    public void setUp() {
        grade = new Grade();

        grade.setEnrolleeId(1);
        grade.setSubjectId(2);
        grade.setValue(12);
        grade.setExamType("diploma");
    }

    @Test
    public void shouldCallNotDefaultConstructor() {
        assertNotNull( grade = new Grade(2, 1, 12, "diploma"));
    }

    @Test
    public void shouldCallToStringGrade() {
        String expected =
                "Grade{subjectId=2, enrolleeId=1, value=12, examType='diploma'}";

        assertEquals(expected, grade.toString());
    }
}
