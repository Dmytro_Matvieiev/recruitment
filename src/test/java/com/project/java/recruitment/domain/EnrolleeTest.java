package com.project.java.recruitment.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EnrolleeTest {
    private Enrollee enrollee;

    @Before
    public void setUp() {
        enrollee = new Enrollee();

        enrollee.setUserId(1);
        enrollee.setCity("Dnipro");
        enrollee.setRegion("Dnipro");
        enrollee.setEducationalInstitution("DNU");
        enrollee.setAttachment(new byte[0]);
        enrollee.setBlocked(false);
    }

    @Test
    public void shouldCallNotDefaultConstructor() {
        assertNotNull(enrollee = new Enrollee("Dnipro", "Dnipro", "school 31", new byte[0], 2));
    }

    @Test
    public void shouldCallToStringEnrollee() {
        String expected =
                "Enrollee{userId=1, city='Dnipro', region='Dnipro', educationalInstitution='DNU', attachment=[], isBlocked=false}";

        assertEquals(expected, enrollee.toString());
    }

    @Test
    public void shouldEqualsToEnrollee() {
        Enrollee enrollee1 = new Enrollee();

        enrollee1.setUserId(1);
        enrollee1.setCity("Dnipro");
        enrollee1.setRegion("Dnipro");
        enrollee1.setEducationalInstitution("DNU");
        enrollee1.setAttachment(new byte[0]);
        enrollee1.setBlocked(false);

        assertTrue(enrollee.equals(enrollee1));
    }

    @Test
    public void shouldReturnUserId() {
        assertEquals(1, enrollee.getUserId());
    }

    @Test
    public void shouldReturnCity() {
        assertEquals("Dnipro", enrollee.getCity());
    }

    @Test
    public void shouldReturnRegion() {
        assertEquals("Dnipro", enrollee.getRegion());
    }

    @Test
    public void shouldReturnEducationalInstitution() {
        assertEquals("DNU", enrollee.getEducationalInstitution());
    }

    @Test
    public void shouldReturnAttachment() {
        assertNotNull(enrollee.getAttachment());
    }

    @Test
    public void testEqualsHashCode() {
        Enrollee enrollee1 = new Enrollee();

        enrollee1.setUserId(1);
        enrollee1.setCity("Dnipro");
        enrollee1.setRegion("Dnipro");
        enrollee1.setEducationalInstitution("DNU");
        enrollee1.setAttachment(new byte[0]);
        enrollee1.setBlocked(false);

        assertTrue(enrollee.equals(enrollee1) && enrollee1.equals(enrollee));
        assertTrue(enrollee.hashCode() == enrollee1.hashCode());
    }

}
