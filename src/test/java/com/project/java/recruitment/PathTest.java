package com.project.java.recruitment;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class PathTest {
    @Test
    public void testConstructorIsPrivate() throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<Path> constructor = Path.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }
}
