package com.project.java.recruitment.config;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DataSourceConnectionTest {

    @Test
    public void testConstructorIsPrivate()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Constructor<DataSourceConnection> constructor = DataSourceConnection.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers())); //this tests that the constructor is private
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @Test
    public void shouldCreateDataSourceConnection() {
        assertNotNull(DataSourceConnection.getMySQLDataSource());
    }
}
