<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="add-client">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="registration.label.enter_info_msg" />
							</div>
						</div>
						<div class="card-body">
							<form id="registration_form" method="POST" action="controller" onsubmit="return validate(this);" autocomplete="off">
								<input type="hidden" name="command" value="client_registration" />
								<div class="form-group row">
									<label for="lang" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.language" />
									</label>
									<div class="col-md-6">
										<select  class="form-control" id="lang" name="lang" required>
											<option value="ru">Russian</option>
											<option value="en">English</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="fullName" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.full_name" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="full_name" id="fullName" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="email" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.email" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="email" id="email" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.password" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="password" name="password" id="password" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="city" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.city" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="city" id="city" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="region" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.region" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="region" id="region" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="educational_institution" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.educational_institution" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="educational_institution" id="educational_institution" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="attachment" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.attachment" />
									</label>
									<div class="col-md-6">
										<input type="file" name="attachment" id="attachment" value=""/>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<button type="reset" class="btn btn-primary btn-block">
											<fmt:message key="registration.button.reset" />
										</button>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<button type="submit" class="btn btn-primary btn-block">
											<fmt:message key="registration.button.submit" />
										</button>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<fmt:message key="registration.label.alredy_registered_msg" />
										<a href="welcome.jsp">
											<fmt:message key="registration.label.login_here_msg" />
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script>
		function validate() {
			var full_name = document.getElementById("full_name");
			var attachment = document.getElementById("attachment");
			var email = document.getElementById("email");
			var password = document.getElementById("password");
			var city = document.getElementById("city");
			var region = document.getElementById("region");
			var educationalInstitution = document.getElementById("educational_institution");

			var valid = true;

			if (full_name.value.length <= 0 || attachment.value.length <= 0) {
				alert("Enter your name and surname!");
			}
			if (email.value.length <= 0 || password.value.length <= 0) {
				alert("Enter your email and password!");
				valid = false;
			}
			if (city.value.length <= 0 || region.value.length <= 0
					|| educationalInstitution.value.length <= 0) {
				alert("Enter your geo data!");
				valid = false;
			}
			return valid;
		};
	</script>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>