<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/profile.css"/>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="edit-client-profile">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="profile.edit_jsp.header" />
							</div>
						</div>
						<div class="card-body">
							<form id="profile" method="POST" action="controller">
								<input type="hidden" name="command" value="editProfile">
								<input type="hidden" name="oldEmail" value="${requestScope.email}">
								<div class="form-group row">
									<label for="lang" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.language" />
									</label>
									<div class="col-md-6">
										<select  class="form-control" id="lang" name="lang" required>
											<option value="ru">Russian</option>
											<option value="en">English</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="fullName" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.full_name" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="full_name" id="fullName" value="${requestScope.full_name}" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="attachment" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.attachment" />
									</label>
									<div class="col-md-6">
										<input type="file" name="attachment" id="attachment" value="${requestScope.attachment}"/>
									</div>
								</div>
								<div class="form-group row">
									<label for="email" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.email" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="email" id="email" value="${requestScope.email}" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.password" />
									</label>
									<div class="col-md-6">
										<input type="password" class="form-control" name="password" id="password" value="${requestScope.password}" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="city" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.city" />
 									</label>
									<div class="col-md-6">
										<input class="form-control" name="city" type="text" value="${requestScope.city}" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="region" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.region" />
									</label>
									<div class="col-md-6">
										<input class="form-control" id="region" name="region" type="text" value="${requestScope.region}" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="educational_institution" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="profile.edit_jsp.label.educational_institution" />
									</label>
									<div class="col-md-6">
										<input class="form-control" name="educational_institution" id="educational_institution"
											   type="text" value="${requestScope.educational_institution}" required />
									</div>
								</div>

								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<button type="submit" class="btn btn-primary">
											<fmt:message key="profile.edit_jsp.button.update" />
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>