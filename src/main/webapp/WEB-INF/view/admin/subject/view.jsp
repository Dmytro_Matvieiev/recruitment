<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/subject.css"/>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="view-subject">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="subject.view_jsp.label.title" />
							</div>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label class="col-md-4 col-form-label text-md-right">
									<fmt:message key="subject.view_jsp.label.name" />
								</label>
								<div class="col-md-6">
									<c:out value="${language eq 'ru' ? name_ru : name_eng}"></c:out>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<input type="button" onclick="location.href='controller?command=editSubject&name_eng=${name_eng}';"
										   value="<fmt:message key="subject.view_jsp.button.edit" />" class="btn btn-primary btn-block">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6 offset-md-4">
									<form id="deleteSubject" action="controller" method="POST">
										<input type="hidden" name="command" value="deleteSubject" />
										<input type="hidden" name="id" value="${id}" />
										<input type="submit" value="<fmt:message key="subject.view_jsp.button.delete" />" class="btn btn-primary btn-block"/>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%--		<c:if test="${not empty errorMessage}">--%>
		<%--			<c:out value="${errorMessage}"></c:out>--%>
		<%--		</c:if>--%>

		<%--		<c:if test="${not empty successfulMessage}">--%>
		<%--			<c:out value="${successfulMessage}"></c:out>--%>
		<%--		</c:if>--%>

	</main>


	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>