<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/faculty.css"/>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="edit-faculty">
		<div class="cotainer">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="faculty.edit_jsp.label.title" />
							</div>
						</div>
						<div class="card-body">
							<form id="edit_faculty" action="controller" method="POST" onsubmit="return validate();">
								<input type="hidden" name="command" value="editFaculty" />
								<input type="hidden" name="oldName" value="${requestScope.name_eng}" />
								<div class="form-group row">
									<label for="name_ru" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.edit_jsp.label.name" /> (ru)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_ru" id="name_ru" value="${requestScope.name_ru}" class="form-control" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="name_eng" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.edit_jsp.label.name" /> (eng)
									</label>
									<div class="col-md-6">
										<input type="text" name="name_eng" id="name_eng" value="${requestScope.name_eng}" class="form-control" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="total_places" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.edit_jsp.label.total_places" />
									</label>
									<div class="col-md-6">
										<input type="number" name="total_places" id="total_places" value="${requestScope.total_places}"
											   class="form-control"  min="1" max="800" step="1" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="budget_places" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.edit_jsp.label.budget_places" />
									</label>
									<div class="col-md-6">
										<input type="number" name="budget_places" id="budget_places" value="${requestScope.budget_places}"
											   class="form-control"  min="1" max="799" step="1" required />
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-4 col-form-label text-md-right">
										<fmt:message key="faculty.edit_jsp.label.entrance_subjects" />
									</label>
									<div class="col-md-6">
										<c:if test="${not empty facultySubjects}">
											<c:forEach var="oldCheckedSubject" items="${facultySubjects}">
												<input type="hidden" name="oldCheckedSubjects" value="${oldCheckedSubject.id}" />
												<p>
													<input type="checkbox" name="subjects" value="${oldCheckedSubject.id}" checked />
													<c:out value="${lang eq 'ru' ? oldCheckedSubject.nameRu : oldCheckedSubject.nameEng}"></c:out>
												</p>
											</c:forEach>
										</c:if>

										<c:if test="${not empty otherSubjects}">
											<c:forEach var="oldUncheckedSubject" items="${otherSubjects}">
												<p>
													<input type="checkbox" name="subjects" value="${oldUncheckedSubject.id}" />
													<c:out value="${lang eq 'ru' ? oldUncheckedSubject.nameRu : oldUncheckedSubject.nameEng}"></c:out>
												</p>
											</c:forEach>
										</c:if>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="button" onclick="location.href='controller?command=viewFaculty&name_eng=${requestScope.name_eng}';"
											   value="<fmt:message key="profile.edit_jsp.button.back" />" class="btn btn-primary btn-block">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<input type="submit" value="<fmt:message key="faculty.edit_jsp.button.submit" />" class="btn btn-primary btn-block"/>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%--		<c:if test="${not empty errorMessage}">--%>
		<%--			<c:out value="${errorMessage}"></c:out>--%>
		<%--		</c:if>--%>

		<%--		<c:if test="${not empty successfulMessage}">--%>
		<%--			<c:out value="${successfulMessage}"></c:out>--%>
		<%--		</c:if>--%>

	</main>


	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/validation/faculty-validation.js"></script>
</body>
</html>