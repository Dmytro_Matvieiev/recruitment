<%@ include file="/WEB-INF/view/jspf/directive/page.jspf"%>
<%@ include file="/WEB-INF/view/jspf/directive/taglib.jspf"%>
<html>
<%@ include file="/WEB-INF/view/jspf/head.jspf"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/profile.css"/>
<body>
	<%@ include file="/WEB-INF/view/jspf/header.jspf"%>

	<main class="add-admin-profile">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="header">
								<fmt:message key="registration.label.enter_admin_info_msg" />
							</div>
						</div>
						<div class="card-body">
							<form id="registration_form" method="POST" action="controller" autocomplete="off">
								<input type="hidden" name="command" value="admin_registration" />
								<div class="form-group row">
									<label for="lang" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.language" />
									</label>
									<div class="col-md-6">
										<select  class="form-control" id="lang" name="lang" required>
											<option value="ru">Russian</option>
											<option value="en">English</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="fullName" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.full_name" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="full_name" id="fullName" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="email" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.email" />
									</label>
									<div class="col-md-6">
										<input class="form-control" type="text" name="email" id="email" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<label for="password" class="col-md-4 col-form-label text-md-right">
										<fmt:message key="registration.label.password" />
									</label>
									<div class="col-md-6">
										<input type="password" class="form-control" name="password" id="password" value="" required />
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<button type="reset" class="btn btn-primary btn-block">
											<fmt:message key="registration.button.reset" />
										</button>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6 offset-md-4">
										<button type="submit" class="btn btn-primary btn-block">
											<fmt:message key="registration.button.submit" />
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>