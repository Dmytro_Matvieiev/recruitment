package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.Fields.ENROLLEE_ATTACHMENT;
import static com.project.java.recruitment.utils.Fields.ENROLLEE_CITY;
import static com.project.java.recruitment.utils.Fields.ENROLLEE_EDUCATIONAL_INSTITUTION;
import static com.project.java.recruitment.utils.Fields.ENROLLEE_IS_BLOCKED;
import static com.project.java.recruitment.utils.Fields.ENROLLEE_REGION;
import static com.project.java.recruitment.utils.Fields.ENROLLEE_USER_ID;
import static com.project.java.recruitment.utils.Fields.ID;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_ENROLLEE;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_ENROLLEES;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_ENROLLEE_FACULTY;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ENROLLEE_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ENROLLEE_BY_USER_ID;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_ENROLLEE;
import static com.project.java.recruitment.utils.SQLQueries.UPDATE_ENROLLEE;

/**
 * Data access object for enrollee related entities.
 * @author - Dmytro
 * @version - 1.0
 */
public class EnrolleeDaoImpl extends DBAbstractDao implements EnrolleeDao {

    private static final Logger LOGGER = Logger.getLogger(EnrolleeDaoImpl.class);

    /**
     * Initialize DataSource object.
     * @param dataSource
     */
    public EnrolleeDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public boolean insertEnrollee(Enrollee enrollee) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_ENROLLEE, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setInt(k++, enrollee.getUserId());
            pstmt.setString(k++, enrollee.getCity());
            pstmt.setString(k++, enrollee.getRegion());
            pstmt.setString(k++, enrollee.getEducationalInstitution());
            pstmt.setBytes(k++, enrollee.getAttachment());
            pstmt.setBoolean(k++, enrollee.isBlocked());


            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetEnrollee(pstmt, enrollee);
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't insert new enrollee", ex);
        }
        return false;
    }

    @Override
    public boolean updateEnrollee(Enrollee enrollee) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(UPDATE_ENROLLEE)) {

            pstmt.setInt(1, enrollee.getUserId());
            pstmt.setString(2, enrollee.getCity());
            pstmt.setString(3, enrollee.getRegion());
            pstmt.setString(4, enrollee.getEducationalInstitution());
            pstmt.setBytes(5, enrollee.getAttachment());
            pstmt.setBoolean(6, enrollee.isBlocked());
            pstmt.setInt(7,  enrollee.getId());

            return pstmt.executeUpdate() > 0; //if successfully update in DB
        } catch (SQLException ex) {
            LOGGER.error("Can't update enrollee", ex);
        }
        return false;
    }

    @Override
    public boolean deleteEnrollee(int id) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_ENROLLEE)) {

            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete enrollee", ex);
        }
        return false;
    }

    @Override
    public Enrollee findEnrollee(int id) {
        Enrollee enrollee = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_ENROLLEE_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                enrollee = mappingEnrollee(rs);
            }
            return enrollee;
        } catch (SQLException ex) {
            LOGGER.error("Can't find enrollee by id: " + id, ex);
        } finally {
            close(rs);
        }
        return enrollee;
    }

    @Override
    public Enrollee findEnrollee(User user) {
        Enrollee enrollee = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_ENROLLEE_BY_USER_ID);) {

            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                enrollee = mappingEnrollee(rs);
            }
            return enrollee;
        } catch (SQLException ex) {
            LOGGER.error("Can't find enrollee by user id: " + user.getId(), ex);
        } finally {
            close(rs);
        }
        return enrollee;
    }

    @Override
    public List<Enrollee> findAllEnrollee() {
        List<Enrollee> enrollees = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_ENROLLEES);) {

            while (rs.next()) {
                enrollees.add(mappingEnrollee(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all enrollees", ex);
        }
        return enrollees;
    }

    @Override
    public List<Enrollee> findAllFacultyEnrollee(Faculty faculty) {
        List<Enrollee> enrollees = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt =  null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(FIND_ALL_ENROLLEE_FACULTY);
            pstmt.setInt(1, faculty.getId());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                enrollees.add(mappingEnrollee(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all faculty enrollees", ex);
        } finally {
            close(rs);
            close(pstmt);
            close(con);
        }
        return enrollees;
    }

    private Enrollee mappingEnrollee(ResultSet rs) {
        Enrollee enrollee = new Enrollee();
        try {
            enrollee.setId(rs.getInt(ID));
            enrollee.setUserId(rs.getInt(ENROLLEE_USER_ID));
            enrollee.setCity(rs.getString(ENROLLEE_CITY));
            enrollee.setRegion(rs.getString(ENROLLEE_REGION));
            enrollee.setEducationalInstitution(rs.getString(ENROLLEE_EDUCATIONAL_INSTITUTION));
            enrollee.setAttachment(rs.getBytes(ENROLLEE_ATTACHMENT));
            enrollee.setBlocked(rs.getBoolean(ENROLLEE_IS_BLOCKED));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for enrollee", ex);
        }
        return enrollee;
    }

    private boolean resultSetEnrollee(PreparedStatement pstmt, Enrollee enrollee) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                enrollee.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close rs in resultSetEnrollee method", ex);
        }
        return false;
    }
}
