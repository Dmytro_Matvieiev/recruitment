package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Grade;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.Fields.ENROLLEE_FK_ID;
import static com.project.java.recruitment.utils.Fields.GRADE_EXAM_TYPE;
import static com.project.java.recruitment.utils.Fields.GRADE_VALUE;
import static com.project.java.recruitment.utils.Fields.ID;
import static com.project.java.recruitment.utils.Fields.SUBJECT_FK_ID;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_GRADE;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_GRADES;
import static com.project.java.recruitment.utils.SQLQueries.FIND_GRADE_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_NEW_GRADE;
import static com.project.java.recruitment.utils.SQLQueries.UPDATE_GRADE;

/**
 * Data access object for grade related entities.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class GradeDaoImpl extends DBAbstractDao implements GradeDao {
    private static final Logger LOGGER = Logger.getLogger(GradeDaoImpl.class);

    /**
     * Initialize DataSource object.
     * @param dataSource
     */
    public GradeDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public boolean insertGrade(Grade grade) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_NEW_GRADE, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setInt(k++, grade.getEnrolleeId());
            pstmt.setInt(k++, grade.getSubjectId());
            pstmt.setInt(k++, grade.getValue());
            pstmt.setString(k++, grade.getExamType());

            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetGrade(pstmt, grade);
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't insert new grade", ex);
        }
        return false;
    }

    @Override
    public boolean updateGrade(Grade grade) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(UPDATE_GRADE)) {

            pstmt.setInt(1, grade.getEnrolleeId());
            pstmt.setInt(2, grade.getSubjectId());
            pstmt.setInt(3, grade.getValue());
            pstmt.setString(4, grade.getExamType());
            pstmt.setInt(5, grade.getId());

            return pstmt.executeUpdate() > 0; //if successfully update in DB
        } catch (Exception ex) {
            LOGGER.error("Can't update grade", ex);
        }
        return false;
    }

    @Override
    public boolean deleteGrade(int id) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_GRADE)) {

            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete grade", ex);
        }
        return false;
    }

    @Override
    public List<Grade> findAll() {
        List<Grade> grades = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_GRADES);) {
            while (rs.next()) {
                grades.add(mappingGrade(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't find all grades", ex);
        }
        return grades;
    }

    @Override
    public Grade find(int id) {
        Grade grade = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_GRADE_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                grade = mappingGrade(rs);
            }
            return grade;
        } catch (SQLException ex) {
            LOGGER.error("Can't find grade by id " + id, ex);
        } finally {
            close(rs);
        }
        return grade;
    }

    private Grade mappingGrade(ResultSet rs) {
        Grade grade = new Grade();
        try {
            grade.setId(rs.getInt(ID));
            grade.setEnrolleeId(rs.getInt(ENROLLEE_FK_ID));
            grade.setSubjectId(rs.getInt(SUBJECT_FK_ID));
            grade.setValue(rs.getInt(GRADE_VALUE));
            grade.setExamType(rs.getString(GRADE_EXAM_TYPE));

        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for grade", ex);
        }
        return grade;
    }

    private boolean resultSetGrade(PreparedStatement pstmt, Grade grade) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                grade.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close result set in resultSetGrade method", ex);
        }
        return false;
    }
}
