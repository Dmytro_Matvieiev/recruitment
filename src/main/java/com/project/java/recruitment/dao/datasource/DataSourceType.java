package com.project.java.recruitment.dao.datasource;

/**
 * DataSources type.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public enum DataSourceType {
    MYSQL_DATASOURCE_WITH_JNDI, MYSQL_DATASOURCE_WITHOUT_JNDI;
}
