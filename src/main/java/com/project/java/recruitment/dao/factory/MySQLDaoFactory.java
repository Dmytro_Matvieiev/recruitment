package com.project.java.recruitment.dao.factory;

import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.EnrolleeDaoImpl;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultyDaoImpl;
import com.project.java.recruitment.dao.FacultyEnrolleesDao;
import com.project.java.recruitment.dao.FacultyEnrolleesDaoImpl;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.FacultySubjectsDaoImpl;
import com.project.java.recruitment.dao.GradeDao;
import com.project.java.recruitment.dao.GradeDaoImpl;
import com.project.java.recruitment.dao.SheetReportDao;
import com.project.java.recruitment.dao.SheetReportDaoImpl;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.SubjectDaoImpl;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.UserDaoImpl;
import com.project.java.recruitment.dao.datasource.DataSourceFactory;
import com.project.java.recruitment.dao.datasource.DataSourceType;

/**
 * MySQLDaoFactory realization of DAO factory.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class MySQLDaoFactory extends DaoFactory {

    private static final MySQLDaoFactory INSTANCE = new MySQLDaoFactory();

    private final FacultyDaoImpl facultyDao = new FacultyDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final FacultyEnrolleesDaoImpl facultyEnrolleesDao = new FacultyEnrolleesDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final FacultySubjectsDaoImpl facultySubjectsDao = new FacultySubjectsDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final UserDaoImpl userDao = new UserDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final EnrolleeDaoImpl enrolleeDao = new EnrolleeDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final SubjectDaoImpl subjectDao = new SubjectDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final GradeDaoImpl gradeDao = new GradeDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private final SheetReportDaoImpl sheetReportDao = new SheetReportDaoImpl(
            DataSourceFactory.getDataSource(DataSourceType.MYSQL_DATASOURCE_WITH_JNDI));

    private MySQLDaoFactory() {
    }

    public static MySQLDaoFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public FacultyDao getFacultyDao() {
        return facultyDao;
    }

    @Override
    public FacultyEnrolleesDao getFacultyEnrolleesDao() {
        return facultyEnrolleesDao;
    }

    @Override
    public FacultySubjectsDao getFacultySubjectsDao() {
        return facultySubjectsDao;
    }

    @Override
    public UserDao getUserDao() {
        return userDao;
    }

    @Override
    public EnrolleeDao getEnrolleeDao() {
        return enrolleeDao;
    }

    @Override
    public SubjectDao getSubjectDao() {
        return subjectDao;
    }

    @Override
    public GradeDao getGradeDao() {
        return gradeDao;
    }

    @Override
    public SheetReportDao getSheetReportDao() {
        return sheetReportDao;
    }
}
