package com.project.java.recruitment.dao.datasource;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * DataSourceFactory class.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public abstract class DataSourceFactory {

    private static final Logger LOGGER = Logger.getLogger(DataSourceFactory.class);

    private DataSourceFactory() {
    }

    public static DataSource getDataSource(DataSourceType type) {
        switch (type) {
            case MYSQL_DATASOURCE_WITHOUT_JNDI:
                Properties props = new Properties();

                try (InputStream fis = DataSourceFactory.class.getResourceAsStream("/application.properties")) {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
                    props.load(fis);
                    dataSource.setURL(props.getProperty("db.url"));
                    dataSource.setUser(props.getProperty("db.user"));
                    dataSource.setPassword(props.getProperty("db.password"));
                    return dataSource;
                } catch (Exception ex) {
                    LOGGER.error("Can't get DataSource without JNDI", ex);
                }
                break;
            case MYSQL_DATASOURCE_WITH_JNDI:
                try {
                    Context context = new InitialContext();
                    return (DataSource) context.lookup("java:/comp/env/jdbc/recruitment");
                } catch (Exception ex) {
                    LOGGER.error("Can'nt get JNDI DataSource", ex);
                }
                break;
            default:
                throw new UnsupportedOperationException("Didn't find DataSource for: " + type);
        }
        return null;
    }
}
