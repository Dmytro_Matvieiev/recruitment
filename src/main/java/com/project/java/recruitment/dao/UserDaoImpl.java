package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.Fields.ID;
import static com.project.java.recruitment.utils.Fields.USER_ACTIVE;
import static com.project.java.recruitment.utils.Fields.USER_EMAIL;
import static com.project.java.recruitment.utils.Fields.USER_FULL_NAME;
import static com.project.java.recruitment.utils.Fields.USER_LANG;
import static com.project.java.recruitment.utils.Fields.USER_PASS;
import static com.project.java.recruitment.utils.Fields.USER_ROLE;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_USER;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_USERS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_USER_BY_EMAIL;
import static com.project.java.recruitment.utils.SQLQueries.FIND_USER_BY_EMAIL_AND_PASS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_USER_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_USER;
import static com.project.java.recruitment.utils.SQLQueries.UPDATE_USER;

/**
 * Data access object for user related entities.
 * @author - Dmytro
 * @version - 1.0
 */
public class UserDaoImpl extends DBAbstractDao implements UserDao {

    private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

    /**
     * Initialize DataSource object.
     * @param dataSource
     */
    public UserDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public List<User> findAllUsers() {
        List<User> users = new ArrayList<>();

        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_USERS);) {
            while (rs.next()) {
                users.add(mappingUsers(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't find all users", ex);
        }
        return users;
    }

    @Override
    public boolean insertUser(User user) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setString(k++, user.getEmail());
            pstmt.setString(k++, user.getPassword());
            pstmt.setString(k++, user.getFullName());
            pstmt.setString(k++, user.getRole());
            pstmt.setString(k++, user.getLang());
            pstmt.setBoolean(k++, user.isActive());


            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetUser(pstmt, user);
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't insert new user", ex);
        }
        return false;
    }

    @Override
    public boolean updateUser(User user) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(UPDATE_USER)) {

            pstmt.setString(1, user.getEmail());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getFullName());
            pstmt.setString(4, user.getRole());
            pstmt.setString(5, user.getLang());
            pstmt.setBoolean(6, user.isActive());
            pstmt.setInt(7, user.getId());

            return pstmt.executeUpdate() > 0; //if successfully update in DB
        } catch (Exception ex) {
            LOGGER.error("Can't update user", ex);
        }
        return false;
    }

    @Override
    public boolean deleteUser(int id) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_USER)) {

            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete user", ex);
        }
        return false;
    }


    @Override
    public User find(String email) {
        User user = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_USER_BY_EMAIL);) {

            pstmt.setString(1, email);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = mappingUsers(rs);
            }
            return user;
        } catch (SQLException ex) {
            LOGGER.error("Can't find user with login " + email, ex);
        } finally {
            close(rs);
        }
        return user;
    }

    @Override
    public User find(String email, String password) {
        User user = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_USER_BY_EMAIL_AND_PASS);) {

            pstmt.setString(1, email);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = mappingUsers(rs);
            }
            return user;
        } catch (SQLException ex) {
            LOGGER.error("Can't find user with login " + email + " and password " + password, ex);
        } finally {
            close(rs);
        }
        return user;
    }

    @Override
    public User find(int id) {
        User user = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_USER_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                user = mappingUsers(rs);
            }
            return user;
        } catch (SQLException ex) {
            LOGGER.error("Can't find user by id " + id, ex);
        } finally {
            close(rs);
        }
        return user;
    }

    private User mappingUsers(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt(1));
            user.setEmail(rs.getString(USER_EMAIL));
            user.setPassword(rs.getString(USER_PASS));
            user.setFullName(rs.getString(USER_FULL_NAME));
            user.setRole(rs.getString(USER_ROLE));
            user.setLang(rs.getString(USER_LANG));
            user.setActive(rs.getBoolean(USER_ACTIVE));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for user", ex);
        }
        return user;
    }

    private boolean resultSetUser(PreparedStatement pstmt, User user) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                user.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close result set in resultSetUser method", ex);
        }
        return false;
    }
}
