package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultySubject;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.Fields.FACULTY_FK_ID;
import static com.project.java.recruitment.utils.Fields.ID;
import static com.project.java.recruitment.utils.Fields.SUBJECT_FK_ID;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_ALL_FACULTY_SUBJECTS;
import static com.project.java.recruitment.utils.SQLQueries.DELETE_FACULTY_SUBJECT;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_FACULTY_SUBJECTS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_FACULTY_SUBJECTS_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_NEW_FACULTY_SUBJECT;

/**
 * Data access object for subjects of faculty related entities.
 * @author - Dmytro
 * @version - 1.0
 */
public class FacultySubjectsDaoImpl extends DBAbstractDao implements FacultySubjectsDao {
    private static final Logger LOGGER = Logger.getLogger(FacultySubjectsDaoImpl.class);

    /**
     * Initialize DataSource object.
     * @param dataSource
     */
    public FacultySubjectsDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public boolean insertFacultySubjects(FacultySubject fs) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(
                     INSERT_NEW_FACULTY_SUBJECT, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setInt(k++, fs.getFacultyId());
            pstmt.setInt(k++, fs.getSubjectId());

            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetFacultySubject(pstmt, fs);
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't insert new faculty subject", ex);
        }
        return false;
    }

    @Override
    public boolean deleteFacultySubjects(FacultySubject fs) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_FACULTY_SUBJECT)) {

            ps.setInt(1, fs.getFacultyId());
            ps.setInt(2, fs.getSubjectId());

            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete faculty subject", ex);
        }
        return false;
    }

    @Override
    public boolean deleteAllFacultySubjects(Faculty faculty) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_ALL_FACULTY_SUBJECTS)) {

            ps.setInt(1, faculty.getId());

            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete faculty all subjects", ex);
        }
        return false;
    }

    @Override
    public List<FacultySubject> findAll() {
        List<FacultySubject> facultySubjects = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_FACULTY_SUBJECTS);) {
            while (rs.next()) {
                facultySubjects.add(mappingFacultySubject(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error("Can't find all faculty subject", ex);
        }
        return facultySubjects;
    }

    @Override
    public FacultySubject find(int id) {
        FacultySubject facultySubject = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_ALL_FACULTY_SUBJECTS_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                facultySubject = mappingFacultySubject(rs);
            }
            return facultySubject;
        } catch (SQLException ex) {
            LOGGER.error("Can't find faculty subjects with id " + id, ex);
        } finally {
            close(rs);
        }
        return facultySubject;
    }

    private FacultySubject mappingFacultySubject(ResultSet rs) {
        FacultySubject facultySubject = new FacultySubject();
        try {
            facultySubject.setId(rs.getInt(ID));
            facultySubject.setFacultyId(rs.getInt(FACULTY_FK_ID));
            facultySubject.setSubjectId(rs.getInt(SUBJECT_FK_ID));

        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for faculty subject", ex);
        }
        return facultySubject;
    }

    private boolean resultSetFacultySubject(PreparedStatement pstmt, FacultySubject fs) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                fs.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close result set in resultSetFacultySubject method", ex);
        }
        return false;
    }
}
