package com.project.java.recruitment.dao.factory;

/**
 * Factory type for DAO factory.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public enum FactoryType {
    MYSQL_DAO_FACTORY;
}
