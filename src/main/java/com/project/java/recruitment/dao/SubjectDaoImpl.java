package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.SQLQueries.DELETE_SUBJECT;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_NOT_FACULTY_SUBJECTS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_ALL_SUBJECTS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_FACULTY_SUBJECTS;
import static com.project.java.recruitment.utils.SQLQueries.FIND_SUBJECT_BY_ID;
import static com.project.java.recruitment.utils.SQLQueries.FIND_SUBJECT_BY_NAME;
import static com.project.java.recruitment.utils.SQLQueries.INSERT_NEW_SUBJECT;
import static com.project.java.recruitment.utils.SQLQueries.UPDATE_SUBJECT;

/**
 * Data access object for subject related entities.
 * @author - Dmytro
 * @version - 1.0
 */
public class SubjectDaoImpl extends DBAbstractDao implements SubjectDao {

    private static final Logger LOGGER = Logger.getLogger(SubjectDaoImpl.class);

    /**
     * Init DataSource object.
     *
     * @param dataSource
     */
    public SubjectDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public boolean insertSubject(Subject subject) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_NEW_SUBJECT, Statement.RETURN_GENERATED_KEYS)) {

            int k = 1;
            pstmt.setString(k++, subject.getNameRu());
            pstmt.setString(k++, subject.getNameEng());

            if (pstmt.executeUpdate() > 0) { //if successfully insert in DB
                return resultSetSubject(pstmt, subject);
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't insert new subject", ex);
        }
        return false;
    }

    @Override
    public boolean updateSubject(Subject subject) {
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(UPDATE_SUBJECT)) {

            pstmt.setString(1, subject.getNameRu());
            pstmt.setString(2, subject.getNameEng());
            pstmt.setInt(3, subject.getId());

            return pstmt.executeUpdate() > 0; //if successfully update in DB
        } catch (SQLException ex) {
            LOGGER.error("Can't update subject", ex);
        }
        return false;
    }

    @Override
    public boolean deleteSubject(int id) {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(DELETE_SUBJECT)) {

            ps.setInt(1, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException ex) {
            LOGGER.error("Can't delete subject", ex);
        }
        return false;
    }

    @Override
    public Subject findSubject(int id) {
        Subject subject = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_SUBJECT_BY_ID);) {

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                subject = mappingSubject(rs);
            }
            return subject;
        } catch (SQLException ex) {
            LOGGER.error("Can't find subject by id -  " + id, ex);
        } finally {
            close(rs);
        }
        return subject;
    }

    @Override
    public List<Subject> findAllSubjects() {
        List<Subject> subjects = new ArrayList<>();
        try (Connection con = getConnection();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_SUBJECTS);) {
            while (rs.next()) {
                subjects.add(mappingSubject(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all subjects", ex);
        }
        return subjects;
    }

    @Override
    public List<Subject> findAllFacultySubjects(Faculty faculty) {
        List<Subject> subjects = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt =  null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(FIND_FACULTY_SUBJECTS);
            pstmt.setInt(1, faculty.getId());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                subjects.add(mappingSubject(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all faculty subjects", ex);
        } finally {
            close(rs);
            close(pstmt);
            close(con);
        }
        return subjects;
    }

    @Override
    public Subject findSubjectByName(String subjectName) {
        Subject subject = null;
        ResultSet rs = null;
        try (Connection con = getConnection();
             PreparedStatement pstmt = con.prepareStatement(FIND_SUBJECT_BY_NAME);) {

            pstmt.setString(1, subjectName);
            pstmt.setString(2, subjectName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                subject = mappingSubject(rs);
            }
            return subject;
        } catch (SQLException e) {
            LOGGER.error("Can't find subject by name - " + subjectName, e);
        } finally {
            close(rs);
        }
        return subject;
    }

    @Override
    public List<Subject> findAllNotFacultySubjects(Faculty faculty) {
        List<Subject> subjects = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt =  null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(FIND_ALL_NOT_FACULTY_SUBJECTS);
            pstmt.setInt(1, faculty.getId());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                subjects.add(mappingSubject(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find all not faculty subjects", ex);
        } finally {
            close(rs);
            close(pstmt);
            close(con);
        }
        return subjects;
    }


    /**
     * Mapping record from faculty_subject table for POJO Subject.
     * @param rs - instance of ResultSet class.
     * @return - Subject instance.
     */
    private Subject mappingSubject(ResultSet rs) {
        Subject subject = new Subject();
        try {
            subject.setId(rs.getInt(Fields.ID));
            subject.setNameRu(rs.getString(Fields.SUBJECT_NAME_RU));
            subject.setNameEng(rs.getString(Fields.SUBJECT_NAME_ENG));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for subject", ex);
        }
        return subject;
    }

    private boolean resultSetSubject(PreparedStatement pstmt, Subject subject) {
        try (ResultSet rs = pstmt.getGeneratedKeys()) { //get id of new row in DB
            if (rs.next()) {
                subject.setId(rs.getInt(1));
                return true;
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't close result set in resultSetSubject method", ex);
        }
        return false;
    }
}
