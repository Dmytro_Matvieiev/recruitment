package com.project.java.recruitment.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * DBAbstractDao class has utils methods.
 * For close AutoCloseable objects and rollback transaction.
 */
public abstract class DBAbstractDao {
    private static final Logger LOGGER = Logger.getLogger(DBAbstractDao.class);
    protected final DataSource dataSource;

    /**
     * Init DataSource object.
     *
     * @param dataSource
     */
    public DBAbstractDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Create connection.
     *
     * @return connection from dataSource.
     * @throws SQLException
     */
    protected Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * Closes this resource, relinquishing any underlying resources.
     *
     * @param ac - object will be close.
     */
    protected void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception ex) {
                LOGGER.error("Can't close a autocloseable", ex);
            }
        }
    }

    /**
     * Rollbacks and closes instance of Connection.
     *
     * @param con - connection that will be rollback and close.
     */
    protected void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                LOGGER.error("Can't rollback a transaction", ex);
            }
        }
    }
}
