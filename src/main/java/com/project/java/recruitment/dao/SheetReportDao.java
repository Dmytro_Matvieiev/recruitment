package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.EnrolleeSheetReport;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface SheetReportDao {

    static final String MESSAGE = "This Repository is implemented as SQL View. Only read operation is allowed.";

    /**
     * Getter for report sheet from database.
     *
     * @param facultyId
     * @return report sheet for faculty which have specified id
     */

    List<EnrolleeSheetReport> getReport(int facultyId);


    default boolean create(EnrolleeSheetReport esr) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    default boolean update(EnrolleeSheetReport esr) {
        throw new UnsupportedOperationException(MESSAGE);
    }

    default boolean delete(EnrolleeSheetReport esr) {
        throw new UnsupportedOperationException(MESSAGE);
    }


    default EnrolleeSheetReport find(int id) {
        throw new UnsupportedOperationException(MESSAGE);
    }


    default List<EnrolleeSheetReport> findAll() {
        throw new UnsupportedOperationException(MESSAGE);
    }
}
