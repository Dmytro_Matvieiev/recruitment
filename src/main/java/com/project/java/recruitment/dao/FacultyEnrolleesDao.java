package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.FacultyEnrollee;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface FacultyEnrolleesDao {
    boolean insertFacultyEnrollee(FacultyEnrollee fe);

    /*
     * Default behavior of update operation is to be not used in implementation.
     * This is the case because Faculty Enrollees is a compound entity, which
     * have foreign keys of Faculty and Enrollee entities.
     */
    default void update(FacultyEnrollee fe) {
        throw new UnsupportedOperationException(
                "This operation is not supported on compound entity 'faculty_enrollee'");
    }
    boolean deleteFacultyEnrollee(FacultyEnrollee fe);
    FacultyEnrollee find(FacultyEnrollee fe);
    FacultyEnrollee findById(int id);
    List<FacultyEnrollee> findAll();
}
