package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface FacultyDao {
    List<Faculty> findAllFaculty();
    boolean insertFaculty(Faculty f);
    boolean updateFaculty(Faculty f);
    boolean deleteFaculty(int id);
    Faculty findFacultyByName(String facultyName);
    Faculty findFacultyById(int id);
}
