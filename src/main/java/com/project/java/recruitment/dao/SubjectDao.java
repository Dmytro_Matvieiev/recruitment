package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.Subject;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface SubjectDao {
    boolean insertSubject(Subject subject);
    boolean updateSubject(Subject subject);
    boolean deleteSubject(int id);
    Subject findSubject(int id);
    List<Subject> findAllSubjects();
    List<Subject> findAllFacultySubjects(Faculty faculty);
    Subject findSubjectByName(String subjectName);
    List<Subject> findAllNotFacultySubjects(Faculty faculty);
}
