package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.EnrolleeSheetReport;
import com.project.java.recruitment.utils.Fields;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import static com.project.java.recruitment.utils.SQLQueries.GET_REPORT_SHEET;

/**
 * Data access object for final sheet report related entities.
 * @author - Dmytro
 * @version - 1.0
 */
public class SheetReportDaoImpl extends DBAbstractDao implements SheetReportDao {

    private static final Logger LOGGER = Logger.getLogger(SheetReportDaoImpl.class);

    /**
     * Initialize DataSource object.
     * @param dataSource
     */
    public SheetReportDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public List<EnrolleeSheetReport> getReport(int facultyId) {
        List<EnrolleeSheetReport> enrolleeSheetReports = new ArrayList<>();
        Connection con = null;
        PreparedStatement pstmt =  null;
        ResultSet rs = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(GET_REPORT_SHEET);
            pstmt.setInt(1, facultyId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                enrolleeSheetReports.add(mappingSheetReport(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error("Can't find enrollee sheet reports", ex);
        } finally {
            close(rs);
            close(pstmt);
            close(con);
        }
        return enrolleeSheetReports;
    }

    private EnrolleeSheetReport mappingSheetReport(ResultSet rs) {
        EnrolleeSheetReport esr = new EnrolleeSheetReport();
        try {
            esr.setFacultyId(rs.getInt(Fields.FACULTY_FK_ID));
            esr.setFullName(rs.getString(Fields.SHEET_REPORT_FULL_NAME));
            esr.setEmail(rs.getString(Fields.SHEET_REPORT_EMAIL));
            esr.setBlocked(rs.getBoolean(Fields.SHEET_REPORT_IS_BLOCKED));
            esr.setDiplomaSum(rs.getInt(Fields.SHEET_REPORT_DIPLOMA_SUM));
            esr.setEntranceSum(rs.getInt(Fields.SHEET_REPORT_ENTRANCE_SUM));
            esr.setTotalSum(rs.getInt(Fields.SHEET_REPORT_TOTAL_SUM));
        } catch (SQLException ex) {
            LOGGER.error("Can't mapping ResultSet for enrollee sheet report", ex);
        }
        return esr;
    }
}
