package com.project.java.recruitment.dao;

import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.User;

import java.util.List;

/**
 * Use Data Access Object (DAO) to abstract and encapsulate data source access.
 * The DAO manages the data source connection to receive and write data.
 * @author - Dmytro
 * @version - 1.0
 */
public interface EnrolleeDao {
    boolean insertEnrollee(Enrollee enrollee);
    boolean updateEnrollee(Enrollee enrollee);
    boolean deleteEnrollee(int id);
    Enrollee findEnrollee(int id);
    Enrollee findEnrollee(User user);
    List<Enrollee> findAllEnrollee();
    List<Enrollee> findAllFacultyEnrollee(Faculty faculty);
}
