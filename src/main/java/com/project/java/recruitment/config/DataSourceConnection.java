package com.project.java.recruitment.config;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.sql.DataSource;

/**
 * MySQL DB config.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class DataSourceConnection {
    private static final Logger LOGGER = Logger.getLogger(DataSourceConnection.class.getName());

    private static MysqlDataSource instance;

    private DataSourceConnection() {
    }

    public static DataSource getMySQLDataSource() {
        Properties props = new Properties();
        if (instance == null) {
            synchronized (DataSourceConnection.class) {
                try (InputStream fis = DataSourceConnection.class.getResourceAsStream("/application.properties")) {
                    props.load(fis);
                    instance = new MysqlConnectionPoolDataSource();
                    instance.setURL(props.getProperty("db.url"));
                    instance.setUser(props.getProperty("db.user"));
                    instance.setPassword(props.getProperty("db.password"));
                } catch (IOException e) {
                    LOGGER.error("IOException in getMySQLDataSource with message - {}", e);
                }
            }
        }
        return instance;
    }
}
