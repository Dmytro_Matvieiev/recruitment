package com.project.java.recruitment.utils;
/**
 * Fields class collects constants for mapping table fields.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class Fields {
    private Fields() {
    }

    public static final String ID = "id";

    /**
     * Fields for Faculty.
     */

    public static final String FACULTY_NAME_RU = "name_ru";
    public static final String FACULTY_NAME_ENG = "name_eng";
    public static final String FACULTY_TOTAL_PLACES = "total_places";
    public static final String FACULTY_BUDGET_PLACES = "budget_places";

    /**
     * Fields for FacultySubject.
     */

    public static final String FACULTY_FK_ID = "faculty_id";

    /**
     * Fields for User.
     */

    public static final String USER_EMAIL = "email";
    public static final String USER_PASS = "password";
    public static final String USER_FULL_NAME = "full_name";
    public static final String USER_ROLE = "role";
    public static final String USER_LANG = "lang";
    public static final String USER_ACTIVE = "is_active";

    /**
     * Fields for Enrollee.
     */

    public static final String ENROLLEE_USER_ID = "user_id";
    public static final String ENROLLEE_CITY = "city";
    public static final String ENROLLEE_REGION = "region";
    public static final String ENROLLEE_EDUCATIONAL_INSTITUTION = "educational_institution";
    public static final String ENROLLEE_IS_BLOCKED = "is_blocked";
    public static final String ENROLLEE_ATTACHMENT = "attachment";

    /**
     * Fields for Subject.
     */

    public static final String SUBJECT_NAME_RU = "name_ru";
    public static final String SUBJECT_NAME_ENG = "name_eng";

    /**
     * Fields for Grade.
     */

    public static final String ENROLLEE_FK_ID = "enrollee_id";
    public static final String SUBJECT_FK_ID = "subject_id";
    public static final String GRADE_VALUE = "value";
    public static final String GRADE_EXAM_TYPE = "exam_type";

    /**
     * Fields for EnrolleeSheetReport.
     */

    public static final String SHEET_REPORT_FULL_NAME = "full_name";
    public static final String SHEET_REPORT_EMAIL = "email";
    public static final String SHEET_REPORT_IS_BLOCKED = "is_blocked";
    public static final String SHEET_REPORT_DIPLOMA_SUM = "diploma_sum";
    public static final String SHEET_REPORT_ENTRANCE_SUM = "entrance_sum";
    public static final String SHEET_REPORT_TOTAL_SUM = "total_sum";
}
