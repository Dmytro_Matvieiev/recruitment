package com.project.java.recruitment.utils;

/**
 * SQLQueries class collects constants with queries to DB.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class SQLQueries {
    private SQLQueries() {
    }

    /**
     * Queries for Faculty.
     */

    public static final String FIND_ALL_FACULTIES = "select * from faculty";
    public static final String FIND_FACULTY_BY_NAME = "select * from faculty where name_ru = ? or name_eng = ?";
    public static final String FIND_FACULTY_BY_ID = "select * from faculty where id = ?";
    public static final String INSERT_NEW_FACULTY =
            "insert into faculty(name_ru, name_eng, total_places, budget_places) values (?, ?, ?, ?)";
    public static final String UPDATE_FACULTY =
            "update faculty set name_ru = ?, name_eng = ?, total_places = ?, budget_places = ? where id = ?";
    public static final String DELETE_FACULTY = "delete from faculty where id = ?";

    /**
     * Queries for User.
     */

    public static final String FIND_ALL_USERS =
            "select id, email, password, full_name, role, lang, is_active from user";

    public static final String FIND_USER_BY_ID =
            "select id, email, password, full_name, role, lang, is_active from user where id = ?";

    public static final String INSERT_USER =
            "insert into user (email, password, full_name, role, lang, is_active) values (?,?,?,?,?,?)";
    public static final String UPDATE_USER =
            "update user set email = ?, password = ?, full_name = ?, role = ?, lang = ?, is_active = ? where id = ?";

    public static final String DELETE_USER = "delete from user where id = ?";

    public static final String FIND_USER_BY_EMAIL_AND_PASS =
            "select id, email, password, full_name, role, lang, is_active from user where email = ? and password = ?;";

    public static final String FIND_USER_BY_EMAIL =
            "select id, email, password, full_name, role, lang, is_active from user where email = ?;";

    /**
     * Queries for Enrollee.
     */

    public static final String INSERT_ENROLLEE =
            "insert into enrollee (user_id, city, region, educational_institution, attachment, is_blocked)  \n" +
                    "values (?,?,?,?,?,?)";

    public static final String UPDATE_ENROLLEE =
            "update enrollee set user_id = ?, city = ?, region = ?, educational_institution = ?,  \n" +
                    "attachment = ?, is_blocked = ? where id = ?";

    public static final String DELETE_ENROLLEE = "delete from enrollee where id = ?";

    public static final String FIND_ENROLLEE_BY_ID =
                    "select id, user_id, city, region, educational_institution, attachment, is_blocked \n" +
                    "from enrollee where id = ?";

    public static final String FIND_ENROLLEE_BY_USER_ID =
                    "select id, user_id, city, region, educational_institution, attachment, is_blocked \n" +
                    "from enrollee where user_id = ?";

    public static final String FIND_ALL_ENROLLEES =
            "select id, user_id, city, region, educational_institution, attachment, is_blocked from enrollee";

    public static final String FIND_ALL_ENROLLEE_FACULTY =
                    "select e.id, e.user_id, e.city, e.region, e.educational_institution, e.attachment,  \n" +
                    "e.is_blocked from enrollee e inner join faculty_enrollee fe on e.id = fe.enrollee_id \n" +
                    "where fe.faculty_id = ?;";

    /**
     * Queries for Subject.
     */
    public static final String INSERT_NEW_SUBJECT = "insert into subject (name_ru, name_eng) values (?,?);";
    public static final String UPDATE_SUBJECT = "update subject set name_ru = ?, name_eng = ? where id = ?;";
    public static final String DELETE_SUBJECT = "delete from subject where id = ?;";
    public static final String FIND_SUBJECT_BY_ID = "select id, name_ru, name_eng from subject where id = ?";
    public static final String FIND_ALL_SUBJECTS = "select id, name_ru, name_eng from subject";
    public static final String FIND_SUBJECT_BY_NAME =
            "select id, name_ru, name_eng from subject where name_ru = ? or name_eng = ? LIMIT 1;";
    public static final String FIND_FACULTY_SUBJECTS = "select s.id, s.name_ru, s.name_eng from subject s \n" +
            "left join faculty_subject f on s.id = f.subject_id where  f.faculty_id = ?";
    public static final String FIND_ALL_NOT_FACULTY_SUBJECTS =
            "select s.id, s.name_ru, s.name_eng from subject s left join faculty_subject f \n" +
            "on  s.id = f.subject_id and f.faculty_id = ? where f.faculty_id is null;";

    /**
     * Queries for FacultySubject.
     */
    public static final String INSERT_NEW_FACULTY_SUBJECT =
            "insert into faculty_subject (faculty_id, subject_id) values (?,?);";
    public static final String DELETE_FACULTY_SUBJECT =
            "delete from faculty_subject where faculty_id = ? and subject_id  = ?";
    public static final String DELETE_ALL_FACULTY_SUBJECTS = "delete from faculty_subject where faculty_id = ?";
    public static final String FIND_ALL_FACULTY_SUBJECTS = "select id, faculty_id, subject_id from faculty_subject";
    public static final String FIND_ALL_FACULTY_SUBJECTS_BY_ID =
            "select id, faculty_id, subject_id from faculty_subject where id = ?";

    /**
     * Queries for Grade.
     */

    public static final String INSERT_NEW_GRADE =
            "insert into grade (enrollee_id, subject_id, value, exam_type) values (?,?,?,?)";
    public static final String UPDATE_GRADE =
            "update grade set enrollee_id = ?, subject_id = ?, value = ?, exam_type = ? where id = ?";
    public static final String DELETE_GRADE = "delete from grade where id = ?";
    public static final String FIND_ALL_GRADES = "select id, enrollee_id, subject_id, value, exam_type from grade";
    public static final String FIND_GRADE_BY_ID =
            "select id, enrollee_id, subject_id, value, exam_type from grade where id = ?";

    /**
     * Queries for FacultyEnrollee.
     */

    public static final String INSERT_NEW_FACULTY_ENROLLEE =
            "insert into faculty_enrollee (faculty_id, enrollee_id) values (?,?)";
    public static final String DELETE_FACULTY_ENROLLEE = "delete from faculty_enrollee where id = ?";
    public static final String FIND_FACULTY_ENROLLEE_BY_FK =
            "select id, faculty_id, enrollee_id from faculty_enrollee where faculty_id = ? and enrollee_id = ?";
    public static final String FIND_FACULTY_ENROLLEE_BY_ID =
            "select id, faculty_id, enrollee_id from faculty_enrollee where id = ?";
    public static final String FIND_ALL_FACULTY_ENROLLEE = "select id, faculty_id, enrollee_id from faculty_enrollee";

    /**
     * Queries for EnrolleeSheetReport.
     */
    public static final String GET_REPORT_SHEET =
            "select faculty_id, full_name, email, is_blocked, diploma_sum, entrance_sum, total_sum \n" +
                    "from faculties_sheet_report";

}
