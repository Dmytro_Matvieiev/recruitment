package com.project.java.recruitment.utils;

/**
 * RequestType class contains type of request from users of portal.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public enum RequestType {
    POST, GET;
}
