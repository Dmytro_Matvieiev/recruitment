package com.project.java.recruitment.utils;

import com.project.java.recruitment.domain.User;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * MailService allows you to send letters to users' email addresses.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class MailService {

    private static final Logger LOGGER = Logger.getLogger(MailService.class);
    private static final Session SESSION = init();
    private static final String confirmSubjectRu = "Подтвердите регистрацию";
    private static final String confirmSubjectEng = "Confirm Registration";
    private static final String confirmationURL =
            "http://localhost:8080/recruitment/controller?command=confirmRegistration&ID=";

    private static final String mailSender = "dmytro.matveev@gmail.com";
    private static final String passwordSender = "dimadima2016";

    /**
     * Method init() initialization session for mail sending.
     * @return
     */
    private static Session init() {
        Session session = null;
        Properties props = new Properties();
        try {
            props.setProperty("mail.transport.protocol", "smtp");
            props.setProperty("mail.host", "smtp.gmail.com");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            props.put("mail.debug", "true");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");

            session = Session.getInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mailSender, passwordSender);
                }
            });

        } catch (Exception ex) {
            LOGGER.error("mail session lookup error", ex);
        }
        return session;
    }

    /**
     * Choose language of letter and send.
     * @param user
     */
    public static void sendConfirmationEmail(User user) {
        try {
            Class.forName("javax.mail.Authenticator");
            Message msg = new MimeMessage(SESSION);
            msg.setFrom(new InternetAddress("dmytro.matveev@gmail.com"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(user.getEmail()));

            if (user.getLang().equals("ru")) {
                setContentToConfirmationEmailRu(msg, user);
            } else {
                setContentToConfirmationEmailEng(msg, user);
            }

            msg.setSentDate(new Date());

            Transport.send(msg);
        } catch (AddressException e) {
            LOGGER.error(e);
        } catch (MessagingException e) {
            LOGGER.error(e);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
        }
    }

    private static void setContentToConfirmationEmailRu(Message msg, User user)
            throws MessagingException, UnsupportedEncodingException {

        msg.setSubject(confirmSubjectRu);

        Multipart multipart = new MimeMultipart();

        String encodedEmail = new String(Base64.getEncoder().encode(
                user.getEmail().getBytes(StandardCharsets.UTF_8)));

        InternetHeaders emailAndPass = new InternetHeaders();
        emailAndPass.addHeader("Content-type", "text/plain; charset=UTF-8");
        String hello = "Здравствуйте, " + user.getFullName() + " !\n"
                + "Вы успешно прошли регистрацию на нашем сайте.\n\n\n";

        String data = "\nВаш логин: " + user.getEmail() + "\nВаш пароль: "
                + user.getPassword() + "\n\n";

        MimeBodyPart greetingAndData = new MimeBodyPart(emailAndPass,
                (hello + data).getBytes("UTF-8"));

        InternetHeaders headers = new InternetHeaders();
        headers.addHeader("Content-type", "text/html; charset=UTF-8");
        String confirmLink = "Подтвердите регистрацию кликнув по этой"
                + "<a href='" + confirmationURL + encodedEmail + "'>ссылке</a>";
        MimeBodyPart link = new MimeBodyPart(headers,
                confirmLink.getBytes(StandardCharsets.UTF_8));

        multipart.addBodyPart(greetingAndData);
        multipart.addBodyPart(link);

        msg.setContent(multipart);
    }

    private static void setContentToConfirmationEmailEng(Message msg, User user)
            throws MessagingException, UnsupportedEncodingException {

        msg.setSubject(confirmSubjectEng);

        Multipart multipart = new MimeMultipart();

        String encodedEmail = new String(Base64.getEncoder().encode(
                user.getEmail().getBytes(StandardCharsets.UTF_8)));

        InternetHeaders emailAndPass = new InternetHeaders();
        emailAndPass.addHeader("Content-type", "text/plain; charset=UTF-8");
        String hello = "Hello, " + user.getFullName() + " !\n"
                + " You successfully registered on our web-site!\n\n\n";

        String data = "\nLogin: " + user.getEmail() + "\nPassword: "
                + user.getPassword() + "\n\n";

        MimeBodyPart greetingAndData = new MimeBodyPart(emailAndPass,
                (hello + data).getBytes("UTF-8"));

        InternetHeaders headers = new InternetHeaders();
        headers.addHeader("Content-type", "text/html; charset=UTF-8");
        String confirmLink = "Complete your registration by clicking on following"
                + "<a href='" + confirmationURL + encodedEmail + "'>link</a>";
        MimeBodyPart link = new MimeBodyPart(headers,
                confirmLink.getBytes("UTF-8"));

        multipart.addBodyPart(greetingAndData);
        multipart.addBodyPart(link);

        msg.setContent(multipart);
    }
}
