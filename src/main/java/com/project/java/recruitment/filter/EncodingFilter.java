package com.project.java.recruitment.filter;

import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Encoding filter.
 * Filter checks client encoding request.
 * Default sets UTF-8.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class EncodingFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);

    private String encoding;

    /**
     * Init filter config.
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Encoding initialization filter start");
        encoding = filterConfig.getInitParameter("encoding");
        LOGGER.trace("Encoding from web.xml --> " + encoding);
        LOGGER.debug("Encoding initialization filter finished");
    }

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        LOGGER.debug("Encoding doFilter start");

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        LOGGER.trace("Request uri --> " + httpRequest.getRequestURI());

        String requestEncoding = request.getCharacterEncoding();
        if (requestEncoding == null) {
            LOGGER.trace("Request encoding = null, set encoding --> " + encoding);
            request.setCharacterEncoding(encoding);
        }

        LOGGER.debug("Encoding doFilter finished");

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.debug("Filter encoding destroy start");
        // no op
        LOGGER.debug("Filter encoding destroy finished");
    }
}
