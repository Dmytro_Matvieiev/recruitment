package com.project.java.recruitment.filter;

import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Security filter.
 * Performs authorization of the user to access
 * resources of the application.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class CommandAccessFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(CommandAccessFilter.class);

    private final List<String> urls;

    /**
     * Accessible to all users.
     */
    private final Set<String> accessibleCommands;

    /**
     * Accessible only to logged in users.
     */
    private final Set<String> commonCommands;

    /**
     * Accessible only for client.
     */
    private final Set<String> clientCommands;

    /**
     * Accessible only for administrator.
     */
    private final Set<String> adminCommands;


    /**
     * Default constructor.
     */
    public CommandAccessFilter() {
        urls = new ArrayList<>();
        accessibleCommands = new HashSet<>();
        commonCommands = new HashSet<>();
        clientCommands = new HashSet<>();
        adminCommands = new HashSet<>();

        accessibleCommands.add("login");
        accessibleCommands.add("viewFaculty");
        accessibleCommands.add("viewAllFaculties");
        accessibleCommands.add("client_registration");
        accessibleCommands.add("confirmRegistration");

        commonCommands.add("logout"); // common commands
        commonCommands.add("viewProfile");
        commonCommands.add("editProfile");

        clientCommands.add("applyFaculty"); // client commands

        adminCommands.add("admin_registration");  // admin commands
        adminCommands.add("editFaculty");
        adminCommands.add("addFaculty");
        adminCommands.add("deleteFaculty");
        adminCommands.add("addSubject");
        adminCommands.add("editSubject");
        adminCommands.add("viewAllSubjects");
        adminCommands.add("viewSubject");
        adminCommands.add("viewEnrollee");
        adminCommands.add("createReport");
        adminCommands.add("deleteSubject");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Start initializing command access filter: " + CommandAccessFilter.class.getSimpleName());
        String avoidURLs = filterConfig.getInitParameter("avoid-urls"); //filter param name
        StringTokenizer token = new StringTokenizer(avoidURLs, ",");

        while (token.hasMoreTokens()) {
            urls.add(token.nextToken());
        }
        LOGGER.debug("Finished initializing command access filter: " + CommandAccessFilter.class.getSimpleName());
    }

    @Override
    public void destroy() {
        LOGGER.debug("Start destroying command access filter: " + CommandAccessFilter.class.getSimpleName());
        // do nothing
        LOGGER.debug("Finished destroying command access filter: " + CommandAccessFilter.class.getSimpleName());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        LOGGER.debug("CommandAccessFilter doFilter start");

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String command = req.getParameter("command");

        if (accessibleCommands.contains(command)) {
            LOGGER.debug("This command can be accessed by all users: " + command);
            chain.doFilter(req, res); // request for accessible url
        } else {
            LOGGER.debug("This command can be accessed only by logged in users: " + command);

            HttpSession session = req.getSession(false);
            if (session == null || session.getAttribute("user") == null) {
                LOGGER.debug("Unauthorized access to resource. Client is not logged-in.");

                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                LOGGER.debug("User is logged-in. Check common commands to logged in users.");
                if (commonCommands.contains(command)) {
                    chain.doFilter(req, res); // Logged-in user found, so just
                } else {

                    LOGGER.debug("Command is specific to user. Check user role.");
                    if ("client".equals(session.getAttribute("userRole"))
                            && clientCommands.contains(command)) {

                        LOGGER.debug("User is client. Command can be executed by client: " + command);
                        chain.doFilter(req, res); // Logged-in user found, so just continue request.
                    } else if ("admin".equals(session.getAttribute("userRole"))
                            && adminCommands.contains(command)) {

                        LOGGER.debug("User is admin. Command can be executed by admin: " + command);
                        chain.doFilter(req, res); // Logged-in user found, so just continue request.
                    } else {
                        res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    }
                }
            }
        }
        LOGGER.debug("CommandAccessFilter doFilter finished");
    }

}
