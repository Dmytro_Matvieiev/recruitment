package com.project.java.recruitment.controller;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.command.CommandContainer;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Front Controller is a servlet whose task is to delegate
 * business model information and processing of its
 * presentation to subcomponents.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class FrontController extends HttpServlet {

    private static final long serialVersionUID = 2423353715955164816L;
    private static final Logger LOGGER = Logger.getLogger(FrontController.class);
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        process(request, response, RequestType.GET);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        process(request, response, RequestType.POST);
    }

    /**
     * Handles all requests coming from the client by executing the specified
     * command name in a request. Implements PRG pattern by checking action type
     * specified by the invoked method.
     *
     * @param request
     * @param response
     * @param requestType
     * @throws IOException
     * @throws ServletException
     * @see RequestType
     */
    private void process(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {

        LOGGER.debug("Start processing in Controller");

        // extract command name from the request
        String commandName = request.getParameter("command");
        LOGGER.trace("Request parameter: 'command' = " + commandName);

        // obtain command object by its name
        Command command = CommandContainer.get(commandName);
        LOGGER.trace("Obtained 'command' = " + command);

        // execute command and get forward address
        String path = command.execute(request, response, requestType);

        if (path == null) {
            LOGGER.trace("Redirect to address = " + path);
            LOGGER.debug("Controller proccessing finished");
            response.sendRedirect(Path.WELCOME_PAGE);
        } else {
            if (requestType == RequestType.GET) {
                LOGGER.trace("Forward to address = " + path);
                LOGGER.debug("Controller proccessing finished");
                RequestDispatcher disp = request.getRequestDispatcher(path);
                disp.forward(request, response);
            } else if (requestType == RequestType.POST) {
                LOGGER.trace("Redirect to address = " + path);
                LOGGER.debug("Controller proccessing finished");
                response.sendRedirect(path);
            }
        }
    }
}
