package com.project.java.recruitment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * CustomTag contains my custom jstl tag.
 * Min grade is 0, max grade is 12.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class CustomTag extends SimpleTagSupport {
    private static final List<Integer> grades = new ArrayList<>();
    private int subjectId;
    private String examType;

    static {
        // populate grades
        for (int i = 0; i <= 12; i++) {
            grades.add(i);
        }
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.println("<select name=\"" + subjectId + "_" + examType + "\">");
        for (Integer grade : grades) {
            out.println("<option>" + grade + "</option>");
        }
        out.println("</select>");
    }
}
