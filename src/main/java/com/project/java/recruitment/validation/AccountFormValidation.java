package com.project.java.recruitment.validation;

/**
 * Data validation in the form of a user account.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class AccountFormValidation {

    private AccountFormValidation() {
    }

    public static boolean validateUserParameters(String fullName, String email, String password, String lang) {
        return FormsFiledValidator.isFilled(fullName, lang)
                && (!email.isEmpty() && email.contains("@"))
                && (password.length() >= 1); //set password length validate
    }

    public static boolean validateEnrolleeParameters(String city, String region, String educationalInstitution) {
        return FormsFiledValidator.isFilled(city, region) && (!educationalInstitution.isEmpty());
    }
}
