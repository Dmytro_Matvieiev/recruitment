package com.project.java.recruitment.validation;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * This class helps to compare the user's entered password with the stored one.
 * Also can help encode and decode password.
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class BCryptValidationPassword {
    private BCryptValidationPassword() {
    }

    /**
     * Retrieves the encoded version of the user's password.
     * @param password
     * @return
     */
    public static String getEncodePassword(String password) {
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }

    /**
     * Compare (decode) password from UI and encode password from DB.
     * @param password
     * @param bcryptHashString
     * @return
     */
    public static boolean isValidPassword(String password, String bcryptHashString) {
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), bcryptHashString);
        return result.verified;
    }
}
