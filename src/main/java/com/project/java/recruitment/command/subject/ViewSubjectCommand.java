package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * View subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewSubjectCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view subject command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        }

        LOGGER.debug("Finished executing view subject command");
        return result;
    }

    /**
     * Forwards admin to the view of some specific subject.
     *
     * @return path to the subject view.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String subjectNameEng = request.getParameter(Fields.SUBJECT_NAME_ENG);

        LOGGER.trace("Subject name to look for is equal to: '" + subjectNameEng + "'");

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        SubjectDao subjectDao = daoFactory.getSubjectDao();
        Subject subject = subjectDao.findSubjectByName(subjectNameEng);

        LOGGER.trace("Subject record found: " + subject);

        request.setAttribute(Fields.ID, subject.getId());
        LOGGER.trace("Set the request attribute: 'id' = " + subject.getId());
        request.setAttribute(Fields.SUBJECT_NAME_RU, subject.getNameRu());
        LOGGER.trace("Set the request attribute: 'name_ru' = " + subject.getNameRu());
        request.setAttribute(Fields.SUBJECT_NAME_ENG, subject.getNameEng());
        LOGGER.trace("Set the request attribute: 'name_eng' = " + subject.getNameEng());
        return Path.FORWARD_SUBJECT_VIEW_ADMIN;
    }

}
