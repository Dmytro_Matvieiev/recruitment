package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Entity;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * View faculty command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewFacultyCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(ViewFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing view faculty command");

        String result = null;
        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else {
            return null;
        }

        LOG.debug("Finished executing view faculty command");

        return result;
    }

    /**
     * Shows page with faculty attributes. Type of action on the page depends on
     * user role.
     *
     * @return path to the view of some faculty.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);

        LOG.trace("Faculty name to look for is equal to: '" + facultyNameEng + "'");

        String result = null;

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        FacultyDao facultyDao = daoFactory.getFacultyDao();

        Faculty facultyRecord = facultyDao.findFacultyByName(facultyNameEng);

        LOG.trace("Faculty record found: " + facultyRecord);

        request.setAttribute(Fields.ID, facultyRecord.getId());
        LOG.trace("Set the request attribute: 'id' = " + facultyRecord.getId());

        request.setAttribute(Fields.FACULTY_NAME_RU, facultyRecord.getNameRu());
        LOG.trace("Set the request attribute: 'name_ru' = " + facultyRecord.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, facultyRecord.getNameEng());
        LOG.trace("Set the request attribute: 'name_eng' = " + facultyRecord.getNameEng());
        request.setAttribute(Fields.FACULTY_TOTAL_PLACES, facultyRecord.getTotalPlaces());
        LOG.trace("Set the request attribute: 'total_places' = " + facultyRecord.getTotalPlaces());
        request.setAttribute(Fields.FACULTY_BUDGET_PLACES, facultyRecord.getBudgetPlaces());
        LOG.trace("Set the request attribute: 'budget_places' = " + facultyRecord.getBudgetPlaces());

        SubjectDao subjectDao = daoFactory.getSubjectDao();

        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(facultyRecord);

        request.setAttribute("facultySubjects", facultySubjects);
        LOG.trace("Set the request attribute: 'facultySubjects' = " + facultySubjects);

        HttpSession session = request.getSession(false);
        String role = (String) session.getAttribute("userRole");

        if (role == null || "client".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_CLIENT;
        } else if ("admin".equals(role)) {
            EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
            List<Enrollee> enrollees = enrolleeDao.findAllFacultyEnrollee(facultyRecord);

            Map<Enrollee, String> facultyEnrollees = new TreeMap<>(
                    Comparator.comparingInt(Entity::getId));

            UserDao userDao = daoFactory.getUserDao();

            for (Enrollee enrollee : enrollees) {
                User user = userDao.find(enrollee.getUserId());
                facultyEnrollees.put(enrollee, user.getFullName());
            }
            request.setAttribute("facultyEnrollees", facultyEnrollees);
            LOG.trace("Set the request attribute: 'facultyEnrollees' = " + facultyEnrollees);

            result = Path.FORWARD_FACULTY_VIEW_ADMIN;
        }

        return result;
    }
}
