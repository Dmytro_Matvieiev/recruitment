package com.project.java.recruitment.command.faculty.result;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.SheetReportDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.EnrolleeSheetReport;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Create faculty's report command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class CreateFacultyReportCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(CreateFacultyReportCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing create faculty's report command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        }

        LOG.debug("Finished executing create faculty's report command");

        return result;
    }

    private String doGet(HttpServletRequest request, HttpServletResponse response) {

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        SheetReportDao sheetReportDao = daoFactory.getSheetReportDao();

        String id = request.getParameter(Fields.ID);
        int facultyId = Integer.parseInt(id);
        System.out.println("facultyId---->"+facultyId);
        List<EnrolleeSheetReport> report = sheetReportDao.getReport(facultyId);

        FacultyDao facultyDao = daoFactory.getFacultyDao();
        Faculty faculty = facultyDao.findFacultyById(facultyId);

        int totalPlaces = faculty.getTotalPlaces();
        int budgetPlaces = faculty.getBudgetPlaces();

        for (int i = 0; i < report.size(); i++) {

            EnrolleeSheetReport sheetReport = report.get(i);

            if ((i < totalPlaces) && (sheetReport.isBlocked() == false)) {

                sheetReport.setEntered(true);

                if (i < budgetPlaces) {
                    sheetReport.setEnteredOnBudget(true);
                } else {
                    sheetReport.setEnteredOnBudget(false);
                }

            } else {
                sheetReport.setEntered(false);
                sheetReport.setEnteredOnBudget(false);
            }
        }

        request.setAttribute(Fields.FACULTY_NAME_RU, faculty.getNameRu());
        LOG.trace("Set attribute 'name_ru': " + faculty.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, faculty.getNameEng());
        LOG.trace("Set attribute 'name_eng': " + faculty.getNameEng());
        request.setAttribute("facultyReport", report);
        LOG.trace("Set attribute 'facultyReport': " + report);

        return Path.FORWARD_REPORT_SHEET_VIEW;
    }
}
