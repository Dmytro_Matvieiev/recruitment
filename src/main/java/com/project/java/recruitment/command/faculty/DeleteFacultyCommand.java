package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete faculty command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class DeleteFacultyCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(DeleteFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing delete faculty command");

        String result = null;

        if (RequestType.POST == requestType) {
            result = doPost(request, response);
        } else {
            result = null;
        }

        LOGGER.debug("Finished executing delete faculty command");

        return result;
    }

    /**
     * Redirects user to view of all faculties after submiting a delete button.
     *
     * @return path to view of all faculties if deletion was successful,
     *         otherwise to faculty view.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        int facultyId = Integer.parseInt(request.getParameter(Fields.ID));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);

        FacultyDao facultyDao = daoFactory.getFacultyDao();
        Faculty facultyToDelete = facultyDao.findFacultyById(facultyId);

        EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();

        List<Enrollee> facultyEnrollee = enrolleeDao.findAllFacultyEnrollee(facultyToDelete);

        if (!facultyEnrollee.isEmpty()) {
            request.setAttribute("errorMessage", "There are records in other tables that rely on this faculty.");
            return Path.REDIRECT_TO_FACULTY + facultyToDelete.getNameEng();
        } else {
            FacultySubjectsDao facultySubjectsDao = daoFactory.getFacultySubjectsDao();

            facultySubjectsDao.deleteAllFacultySubjects(facultyToDelete);
            LOGGER.trace("Delete entrance subjects records in database of a faculty: " + facultyToDelete);

            facultyDao.deleteFaculty(facultyToDelete.getId());

            LOGGER.trace("Delete faculty record in database: " + facultyToDelete);
            return Path.REDIRECT_TO_VIEW_ALL_FACULTIES;
        }
    }
}
