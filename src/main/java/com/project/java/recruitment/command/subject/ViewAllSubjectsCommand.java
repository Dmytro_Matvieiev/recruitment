package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * View all subjects command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewAllSubjectsCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewAllSubjectsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view all subjects command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        }

        LOGGER.debug("Finished executing view all subjects command");
        return result;
    }

    /**
     * Forwards admin to the view of all subjects.
     *
     * @return path to all subjects view
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        SubjectDao subjectDao = daoFactory.getSubjectDao();

        List<Subject> allSubjects = subjectDao.findAllSubjects();

        LOGGER.trace("Subjects records found: " + allSubjects);

        request.setAttribute("allSubjects", allSubjects);
        LOGGER.trace("Set the request attribute: 'allSubjects' = " + allSubjects);

        return Path.FORWARD_SUBJECT_VIEW_ALL_ADMIN;
    }
}
