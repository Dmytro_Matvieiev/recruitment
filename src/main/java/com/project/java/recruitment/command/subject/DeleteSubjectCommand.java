package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.FacultySubject;
import com.project.java.recruitment.domain.Grade;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class DeleteSubjectCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(DeleteSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing delete subject command");

        String result = null;

        if (RequestType.POST == requestType) {
            result = doPost(request, response);
        } else {
            result = null;
        }

        LOGGER.debug("Finished executing delete subject command");

        return result;
    }

    /**
     * Redirects user to view of all subjects after submiting a delete button.
     *
     * @return path to view of all subjects if deletion was successful,
     *         otherwise to subject view.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        int subjectId = Integer.parseInt(request.getParameter(Fields.ID));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        SubjectDao subjectDao = daoFactory.getSubjectDao();
        Subject subjectToDelete = subjectDao.findSubject(subjectId);

        LOGGER.trace("Found subject that should be deleted: " + subjectToDelete);

        FacultySubjectsDao facultySubjectsDao = daoFactory.getFacultySubjectsDao();

        List<FacultySubject> facultySubjects = facultySubjectsDao.findAll();

        facultySubjects
                .removeIf(record -> record.getSubjectId() != subjectToDelete
                        .getId());

        String result = null;

        if (facultySubjects.isEmpty()) {
            LOGGER.trace("No faculties have this subject as entrance. Check enrollee grades.");
            List<Grade> grades = daoFactory.getGradeDao().findAll();

            grades.removeIf(record -> record.getSubjectId() != subjectToDelete.getId());

            if (grades.isEmpty()) {
                LOGGER.trace("No grades records on this subject. Perform deleting.");
                subjectDao.deleteSubject(subjectToDelete.getId());
                result = Path.REDIRECT_TO_VIEW_ALL_SUBJECTS;
            } else {
                LOGGER.trace("There are grades records that rely on this subject.");
                result = Path.REDIRECT_TO_SUBJECT + subjectToDelete.getNameEng();
            }

        } else {
            LOGGER.trace("There are faculties that have this subject as entrance.");
            result = Path.REDIRECT_TO_SUBJECT + subjectToDelete.getNameEng();
        }
        return result;
    }
}
