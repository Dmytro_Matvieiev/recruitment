package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultyEnrolleesDao;
import com.project.java.recruitment.dao.GradeDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultyEnrollee;
import com.project.java.recruitment.domain.Grade;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Applying for admission to the faculty.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ApplyFacultyViewCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ApplyFacultyViewCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {

        LOGGER.debug("Start executing apply faculty view command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing apply faculty view command");
        return result;
    }

    /**
     * Forwards user to apply page of interested faculty.
     *
     * @return path to apply for faculty page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);

        FacultyDao facultyDao = daoFactory.getFacultyDao();

        Faculty faculty = facultyDao.findFacultyByName(facultyNameEng);

        request.setAttribute(Fields.ID, faculty.getId());
        LOGGER.trace("Set the request faculty attribute: 'id' = " + faculty.getId());

        request.setAttribute(Fields.FACULTY_NAME_RU, faculty.getNameRu());
        LOGGER.trace("Set the request attribute: 'name' = " + faculty.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, faculty.getNameEng());
        LOGGER.trace("Set the request attribute: 'name_eng' = " + faculty.getNameEng());
        request.setAttribute(Fields.FACULTY_TOTAL_PLACES, faculty.getTotalPlaces());
        LOGGER.trace("Set the request attribute: 'total_places' = " + faculty.getTotalPlaces());
        request.setAttribute(Fields.FACULTY_BUDGET_PLACES, faculty.getBudgetPlaces());
        LOGGER.trace("Set the request attribute: 'budget_places' = " + faculty.getBudgetPlaces());

        SubjectDao subjectDao = daoFactory.getSubjectDao();

        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(faculty);
        request.setAttribute("facultySubjects", facultySubjects);
        LOGGER.trace("Set attribute 'facultySubjects': " + facultySubjects);

        List<Subject> allSubjects = subjectDao.findAllSubjects();
        request.setAttribute("allSubjects", allSubjects);
        LOGGER.trace("Set attribute 'allSubjects': " + allSubjects);

        result = Path.FORWARD_FACULTY_APPLY_CLIENT;
        return result;
    }

    /**
     * @return redirects user to view of applied faculty if applying is
     *         successful, otherwise redisplays this page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.trace("Start processing applying for faculty form");

        HttpSession session = request.getSession(false);
        String email = String.valueOf(session.getAttribute("user"));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);

        UserDao userDao = daoFactory.getUserDao();

        User user = userDao.find(email);
        LOGGER.trace("Found user in database that wants to apply: " + user);

        EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
        Enrollee enrollee = enrolleeDao.findEnrollee(user);

        LOGGER.trace("Found enrollee record in database for this user: " + enrollee);

        FacultyEnrolleesDao facultyEnrolleesDao = daoFactory.getFacultyEnrolleesDao();

        Integer facultyId = Integer.valueOf(request.getParameter(Fields.ID));

        FacultyEnrollee newFacultyEnrollee = new FacultyEnrollee(facultyId, enrollee.getId());
        FacultyEnrollee existingRecord = facultyEnrolleesDao.find(newFacultyEnrollee);

        if (existingRecord != null) {
            // user is already applied
            LOGGER.trace("User: " + user + " with Enrollee record: " + enrollee +
                    " already applied for faculty with id: " + facultyId);
            return Path.REDIRECT_TO_VIEW_ALL_FACULTIES;
        } else {

            LOGGER.trace("Start extracting data from request in apply faculty view command");

            Map<String, String[]> parameterMap = request.getParameterMap();
            GradeDao gradeDao = daoFactory.getGradeDao();


//            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
//                System.out.println(entry.getKey() + ":" + entry.getValue());
//
//
//            }

            for (String parameterName : parameterMap.keySet()) {

                if (parameterName.endsWith("entrance") || parameterName.endsWith("diploma")) {
                    String[] value = parameterMap.get(parameterName);
                    int gradeValue = Integer.parseInt(value[0]);
                    String[] subjectIdAndExamType = parameterName.split("_");

                    Integer subjectId = Integer.valueOf(subjectIdAndExamType[0]);
                    String examType = subjectIdAndExamType[1];

                    Grade grade = new Grade(subjectId, enrollee.getId(), gradeValue, examType);
                    LOGGER.trace("Create Grade transfer object: " + grade);

                    gradeDao.insertGrade(grade);

                    LOGGER.trace("Grade record was created in database: " + grade);
                }
            }

            LOGGER.trace("End extracting data from request in apply faculty view command");

            LOGGER.trace("Create FacultyEnrolles transfer object: " + newFacultyEnrollee);

            facultyEnrolleesDao.insertFacultyEnrollee(newFacultyEnrollee);

            LOGGER.trace("FacultyEnrolles record was created in database: " + newFacultyEnrollee);

            LOGGER.trace("Finished processing applying for faculty form");

            FacultyDao facultyDao = daoFactory.getFacultyDao();
            Faculty faculty = facultyDao.findFacultyById(facultyId);
            return Path.REDIRECT_TO_FACULTY + faculty.getNameEng();
        }
    }
}
