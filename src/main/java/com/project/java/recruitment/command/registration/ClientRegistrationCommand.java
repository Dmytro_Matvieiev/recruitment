package com.project.java.recruitment.command.registration;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.Role;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.MailService;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.AccountFormValidation;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Add new client command.
 * Serves for registration new client in system.
 * @author - Dmytro
 * @version - 1.0
 */
public class ClientRegistrationCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ClientRegistrationCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing client registration command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing client registration command");

        return result;
    }

    /**
     * Forwards user to client registration page.
     * @return path of the page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        return Path.FORWARD_CLIENT_REGISTRATION_PAGE;
    }

    /**
     * Register a new user if all fields are filled in correctly.
     * @return the user gets to the welcome page as a result of a successful
     * registration, otherwise we display the registration page again.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            LOGGER.debug("Start executing registration command");
            String email = request.getParameter(Fields.USER_EMAIL);
            String password = request.getParameter(Fields.USER_PASS);
            String fullName = request.getParameter(Fields.USER_FULL_NAME);
            String lang = request.getParameter(Fields.USER_LANG);

            String city = request.getParameter(Fields.ENROLLEE_CITY);
            String region = request.getParameter(Fields.ENROLLEE_REGION);
            String educationalInstitution = request.getParameter(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION);
            byte[] attachment = request.getParameter(Fields.ENROLLEE_ATTACHMENT).getBytes();

            boolean valid = AccountFormValidation.validateUserParameters(fullName, email, password, lang);
            LOGGER.trace(valid);

            valid = AccountFormValidation.validateEnrolleeParameters(city, region, educationalInstitution);
            if (valid) {
                User user = new User(email, password, fullName, Role.CLIENT, lang, false);
                DaoFactory daoFactory = DaoFactory
                        .getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
//
                UserDao userDao = daoFactory.getUserDao();
                boolean isSaveUser = userDao.insertUser(user);
                if (isSaveUser) {
                    LOGGER.trace("User was successfully created: " + user + "!");
                    Enrollee enrollee = new Enrollee(city, region, educationalInstitution, attachment, user);

                    EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
                    boolean isSaveEnrollee = enrolleeDao.insertEnrollee(enrollee);

                    if (isSaveEnrollee) {
                        LOGGER.trace("Enrollee was successfully created: " + enrollee + "!");

                        request.setAttribute("successfulMessage",
                                "Your account was created. Check your email and confirm your registration.");
                    } else {
                        LOGGER.trace("Enrollee wasn't created: " + enrollee + "!");

                        request.setAttribute("errorMessage", "Failed to save new enrollee!");
                    }

                } else {
                    LOGGER.trace("User wasn't created: " + user + "!");

                    request.setAttribute("errorMessage", "Failed to save new user!");
                }

                /**
                 * java.util.ServiceConfigurationError: javax.mail.Provider:
                 * Provider com.sun.mail.imap.IMAPProvider not a subtype
                 */
//                MailService.sendConfirmationEmail(user);

                return Path.WELCOME_PAGE;
            } else {
                request.setAttribute("errorMessage", "Please fill all fields!");
                LOGGER.error("errorMessage: Not all fields are filled");
                return Path.REDIRECT_CLIENT_REGISTRATION_PAGE;
            }
        } catch (Exception ex) {
            LOGGER.error("Message: Something went wrong!");
            return Path.ERROR_PAGE;
        }

    }
}
