package com.project.java.recruitment.command.subject;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.SubjectInputValidator;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Add subject command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class AddSubjectCommand extends Command {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(AddSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing add subject command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing add subject command");
        return result;
    }

    /**
     * Forwards admin to add page.
     *
     * @return path to add page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        return Path.FORWARD_SUBJECT_ADD_ADMIN;
    }

    /**
     * Adds subject if fields are properly filled, otherwise redisplays add
     * page.
     *
     * @return view of added subject
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String nameRu = request.getParameter("name_ru");
        LOGGER.trace("Fetch request parameter: 'name_ru' = " + nameRu);

        String nameEng = request.getParameter("name_eng");
        LOGGER.trace("Fetch request parameter: 'name_eng' = " + nameEng);

        boolean valid = SubjectInputValidator.validateParameters(nameRu, nameEng);

        String result = null;
        if (valid) {
            DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
            SubjectDao subjectRepository = daoFactory.getSubjectDao();

            Subject subject = new Subject();
            subject.setNameRu(nameRu);
            subject.setNameEng(nameEng);

            LOGGER.trace("Create subject transfer object: " + subject);

            subjectRepository.insertSubject(subject);
            LOGGER.trace("Create subject record in database: " + subject);
            result = Path.REDIRECT_TO_SUBJECT + nameEng;
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_SUBJECT_ADD_ADMIN;
        }
            return result;
    }
}
