package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultySubject;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.FacultyInputValidator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Edit faculty command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class EditFacultyCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(EditFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing edit faculty command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing edit faculty command");
        return result;

    }

    /**
     * Forwards to the edit faculty page.
     *
     * @return path to edit page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String facultyName = request.getParameter(Fields.FACULTY_NAME_ENG);

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        FacultyDao facultyDao = daoFactory.getFacultyDao();
        Faculty faculty = facultyDao.findFacultyByName(facultyName);

        request.setAttribute(Fields.FACULTY_NAME_RU, faculty.getNameRu());
        LOGGER.trace("Set attribute 'name_ru': " + faculty.getNameRu());
        request.setAttribute(Fields.FACULTY_NAME_ENG, faculty.getNameEng());
        LOGGER.trace("Set attribute 'name_eng': " + faculty.getNameEng());
        request.setAttribute(Fields.FACULTY_TOTAL_PLACES, faculty.getTotalPlaces());
        LOGGER.trace("Set attribute 'total_places': " + faculty.getTotalPlaces());
        request.setAttribute(Fields.FACULTY_BUDGET_PLACES, faculty.getBudgetPlaces());
        LOGGER.trace("Set attribute 'budget_places': " + faculty.getBudgetPlaces());

        SubjectDao subjectDao = daoFactory.getSubjectDao();

        List<Subject> otherSubjects = subjectDao.findAllNotFacultySubjects(faculty);
        request.setAttribute("otherSubjects", otherSubjects);
        LOGGER.trace("Set attribute 'otherSubjects': " + otherSubjects);

        List<Subject> facultySubjects = subjectDao.findAllFacultySubjects(faculty);
        request.setAttribute("facultySubjects", facultySubjects);
        LOGGER.trace("Set attribute 'facultySubjects': " + facultySubjects);

        return Path.FORWARD_FACULTY_EDIT_ADMIN;
    }

    /**
     * Edits faculty according to entered data by admin.
     *
     * @return path to the view of edited faculty if succeeded, otherwise
     *         redisplays page with <code>doGet</code>
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        // get parameters from page

        String facultyNameRu = request.getParameter(Fields.FACULTY_NAME_RU);
        LOGGER.trace("Get parameter 'name_ru' = " + facultyNameRu);
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);
        LOGGER.trace("Get parameter 'name_eng' = " + facultyNameEng);
        String facultyTotalPlaces = request.getParameter(Fields.FACULTY_TOTAL_PLACES);
        LOGGER.trace("Get parameter 'total_places' = " + facultyTotalPlaces);
        String facultyBudgetPlaces = request.getParameter(Fields.FACULTY_BUDGET_PLACES);
        LOGGER.trace("Get parameter 'budget_places' = " + facultyBudgetPlaces);
        // if user changes faculty name we need to know the old one
        // to update record in db
        String oldFacultyName = request.getParameter("oldName");
        LOGGER.trace("Get old faculty name from page: " + oldFacultyName);

        boolean valid = FacultyInputValidator.validateParameters(facultyNameRu,
                facultyNameEng, facultyBudgetPlaces, facultyTotalPlaces);

        if (valid) {
            // if it's true then let's start to update the db

            LOGGER.trace("All fields are properly filled. Start updating database.");

            int totalPlaces = Integer.parseInt(facultyTotalPlaces);
            int budgetPlaces = Integer.parseInt(facultyBudgetPlaces);

            Faculty faculty = new Faculty(facultyNameRu, facultyNameEng, totalPlaces, budgetPlaces);

            DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
            FacultyDao facultyDao = daoFactory.getFacultyDao();

            Faculty oldFacultyRecord = facultyDao.findFacultyByName(oldFacultyName);

            faculty.setId(oldFacultyRecord.getId());

            facultyDao.updateFaculty(faculty);

            LOGGER.trace("Faculty record updated from: " + oldFacultyRecord + ", to: " + faculty);

            String[] oldCheckedSubjectIds = request.getParameterValues("oldCheckedSubjects");

            LOGGER.trace("Get checked subjects before: " + Arrays.toString(oldCheckedSubjectIds));

            String[] newCheckedSubjectsIds = request.getParameterValues("subjects");

            LOGGER.trace("Get checked subjects after: " + Arrays.toString(newCheckedSubjectsIds));

            FacultySubjectsDao facultySubjectsDao = daoFactory.getFacultySubjectsDao();

            if (oldCheckedSubjectIds == null) {
                if (newCheckedSubjectsIds == null) {
                    // if before all subjects were unchecked and they are still
                    // are
                    // then nothing changed - do nothing
                    LOGGER.trace("No faculty subjects records will be changed");
                } else {
                    // if user checked something,but before no subjects were
                    // checked
                    for (String newCheckedSubject : newCheckedSubjectsIds) {
                        int subjectId = Integer.parseInt(newCheckedSubject);
                        FacultySubject facultySubject = new FacultySubject(subjectId, faculty.getId());
                        facultySubjectsDao.insertFacultySubjects(facultySubject);
                        LOGGER.trace("Faculty subjects record was created: " + facultySubject);
                    }
                }
            }

            if (oldCheckedSubjectIds != null) {
                if (newCheckedSubjectsIds == null) {
                    // if user made unchecked all checkbox's and before
                    // there
                    // were some checked subjects
                    LOGGER.trace("No subjects were checked for this faculty - " +
                            "all records that will be found will be deleted ");
                    facultySubjectsDao.deleteAllFacultySubjects(faculty);
                } else {
                    // if there were checked subjects and still are
                    // then for INSERT we should check if the record already exists in db

                    Set<String> existingRecords = new HashSet<>(Arrays.asList(oldCheckedSubjectIds));

                    for (String newCheckedSubject : newCheckedSubjectsIds) {
                        if (existingRecords.contains(newCheckedSubject)) {
                            // if exists - then do nothing
                            LOGGER.trace("This faculty subjects records already exists in db: "
                                    + "facultyId = " + faculty.getId()
                                    + ", subjectId = " + newCheckedSubject);
                        } else {
                            // otherwise INSERT
                            int subjectId = Integer.parseInt(newCheckedSubject);
                            FacultySubject facultySubject = new FacultySubject(subjectId, faculty.getId());
                            facultySubjectsDao.insertFacultySubjects(facultySubject);
                            LOGGER.trace("Faculty subjects record was created: " + facultySubject);
                        }
                    }

                    // and check for DELETE records that were previously
                    // checked and now are not
                    Set<String> newRecords = new HashSet<>(Arrays.asList(newCheckedSubjectsIds));

                    existingRecords.removeIf(subject -> newRecords
                            .contains(subject));

                    if (!existingRecords.isEmpty()) {
                        for (String subjectToRemove : existingRecords) {
                            int subjectId = Integer.parseInt(subjectToRemove);
                            FacultySubject facultySubjectRecordToDelete =
                                    new FacultySubject(subjectId, faculty.getId());
                            facultySubjectsDao.deleteFacultySubjects(facultySubjectRecordToDelete);
                            LOGGER.trace("Faculty subjects record was deleted: " + facultySubjectRecordToDelete);
                        }
                    }
                }
            }
            result = Path.REDIRECT_TO_FACULTY + facultyNameEng;
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");

            result = Path.REDIRECT_FACULTY_EDIT_ADMIN + oldFacultyName;
        }
        return result;
    }
}
