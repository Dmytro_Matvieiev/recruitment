package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * View user profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewProfileCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewProfileCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view profile command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        }

        LOGGER.debug("Finished executing view profile command");

        return result;
    }

    /**
     * Forwards user to his profile page, based on his role.
     *
     * @return path to user profile
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        HttpSession session = request.getSession(false);
        String userEmail = String.valueOf(session.getAttribute("user"));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        UserDao userDao = daoFactory.getUserDao();
        // should not be null !
        User user = userDao.find(userEmail);

        request.setAttribute("full_name", user.getFullName());
        LOGGER.trace("Set the request attribute: 'full_name' = " + user.getFullName());
        request.setAttribute("email", user.getEmail());
        LOGGER.trace("Set the request attribute: 'email' = " + user.getEmail());
        request.setAttribute("role", user.getRole());
        LOGGER.trace("Set the request attribute: 'role' = " + user.getRole());

        String role = user.getRole();

        if ("client".equals(role)) {

            EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
            // should not be null !!
            Enrollee enrollee = enrolleeDao.findEnrollee(user);

            request.setAttribute("city", enrollee.getCity());
            LOGGER.trace("Set the request attribute: 'city' = " + enrollee.getCity());
            request.setAttribute("region", enrollee.getRegion());
            LOGGER.trace("Set the request attribute: 'region' = " + enrollee.getRegion());
            request.setAttribute("attachment", enrollee.getAttachment());
            LOGGER.trace("Set the request attribute: 'attachment' = " + enrollee.getAttachment());
            request.setAttribute("educational_institution", enrollee.getEducationalInstitution());
            LOGGER.trace("Set the request attribute: 'educational_institution' = "
                    + enrollee.getEducationalInstitution());
            request.setAttribute("is_blocked", enrollee.isBlocked());
            LOGGER.trace("Set the request attribute: 'is_blocked' = " + enrollee.isBlocked());

            result = Path.FORWARD_CLIENT_PROFILE;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_ADMIN_PROFILE;
        }
        return result;
    }
}
