package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.FacultySubjectsDao;
import com.project.java.recruitment.dao.SubjectDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.domain.FacultySubject;
import com.project.java.recruitment.domain.Subject;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.FacultyInputValidator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Add new faculty command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class AddFacultyCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(AddFacultyCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing add faculty command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing add faculty command");

        return result;
    }

    /**
     * Forwards to add page.
     * @return path to the add new faculty page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.trace("Request for only showing (not already adding) faculty/add.jsp");

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        SubjectDao subjectDao = daoFactory.getSubjectDao();

        List<Subject> allSubjects = subjectDao.findAllSubjects();
        LOGGER.trace("All subjects found: " + allSubjects);
        request.setAttribute("allSubjects", allSubjects);
        LOGGER.trace("Set request attribute 'allSubjects' = " + allSubjects);

        return Path.FORWARD_FACULTY_ADD_ADMIN;
    }

    /**
     * Redirects user after submitting add faculty form.
     *
     * @return path to the view of added faculty if fields properly filled,
     *         otherwise redisplays add Faculty page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        String facultyNameRu = request.getParameter(Fields.FACULTY_NAME_RU);
        String facultyNameEng = request.getParameter(Fields.FACULTY_NAME_ENG);
        String facultyTotalPlaces = request.getParameter(Fields.FACULTY_TOTAL_PLACES);
        String facultyBudgetPlaces = request.getParameter(Fields.FACULTY_BUDGET_PLACES);

        boolean valid = FacultyInputValidator.validateParameters(facultyNameRu,
                facultyNameEng, facultyBudgetPlaces, facultyTotalPlaces);

        if (valid) {

            LOGGER.trace("All fields are properly filled. Starting update database.");

            Integer totalPlaces = Integer.valueOf(facultyTotalPlaces);
            Integer budgetPlaces = Integer.valueOf(facultyBudgetPlaces);

            Faculty faculty = new Faculty(facultyNameRu, facultyNameEng, totalPlaces, budgetPlaces);

            LOGGER.trace("Create faculty transfer object: " + faculty);

            DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);

            FacultyDao facultyDao = daoFactory.getFacultyDao();

            facultyDao.insertFaculty(faculty);

            LOGGER.trace("Create faculty record in database: " + faculty);

            // only after creating a faculty record we can proceed with
            // adding faculty subjects
            String[] choosedSubjectsIds = request.getParameterValues("subjects");

            if (choosedSubjectsIds != null) {
                FacultySubjectsDao facultySubjectsDao = daoFactory.getFacultySubjectsDao();

                for (String subjectId : choosedSubjectsIds) {
                    FacultySubject facultySubject = new FacultySubject(Integer.valueOf(subjectId), faculty.getId());
                    facultySubjectsDao.insertFacultySubjects(facultySubject);

                    LOGGER.trace("FacultySubjects was successfully recorded in DB: " + facultySubject);
                }
            }
            result = Path.REDIRECT_TO_FACULTY + facultyNameEng;
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_FACULTY_ADD_ADMIN;
        }
        return result;
    }
}
