package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.FacultyDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Faculty;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * View a list of all faculties command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewAllFacultiesCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ViewAllFacultiesCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing view all faculty command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else {
            result = null;
        }

        LOGGER.debug("Finished executing view all faculty command");
        return result;
    }

    /**
     * Forward user to page of all faculties. View type depends on the user
     * role.
     *
     * @return to view of all facultues
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        FacultyDao facultyDao = daoFactory.getFacultyDao();

        List<Faculty> faculties = facultyDao.findAllFaculty();

        LOGGER.trace("Faculties records found: " + faculties);

        request.setAttribute("faculties", faculties);
        LOGGER.trace("Set the request attribute: 'faculties' = " + faculties);

        HttpSession session = request.getSession(false);
        String role = (String) session.getAttribute("userRole");

        if (role == null || "client".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_ALL_CLIENT;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_FACULTY_VIEW_ALL_ADMIN;
        }

        return result;
    }
}
