package com.project.java.recruitment.command;

import com.project.java.recruitment.command.account.EditProfileCommand;
import com.project.java.recruitment.command.account.LoginCommand;
import com.project.java.recruitment.command.account.LogoutCommand;
import com.project.java.recruitment.command.account.ViewProfileCommand;
import com.project.java.recruitment.command.faculty.AddFacultyCommand;
import com.project.java.recruitment.command.faculty.ApplyFacultyViewCommand;
import com.project.java.recruitment.command.faculty.DeleteFacultyCommand;
import com.project.java.recruitment.command.faculty.EditFacultyCommand;
import com.project.java.recruitment.command.faculty.ViewAllFacultiesCommand;
import com.project.java.recruitment.command.faculty.ViewEnrolleeCommand;
import com.project.java.recruitment.command.faculty.ViewFacultyCommand;
import com.project.java.recruitment.command.faculty.result.CreateFacultyReportCommand;
import com.project.java.recruitment.command.registration.AdminRegistrationCommand;
import com.project.java.recruitment.command.registration.ClientRegistrationCommand;
import com.project.java.recruitment.command.registration.ConfirmRegistrationCommand;
import com.project.java.recruitment.command.subject.AddSubjectCommand;
import com.project.java.recruitment.command.subject.DeleteSubjectCommand;
import com.project.java.recruitment.command.subject.EditSubjectCommand;
import com.project.java.recruitment.command.subject.ViewAllSubjectsCommand;
import com.project.java.recruitment.command.subject.ViewSubjectCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * CommandManager class work with all commands from users of portal.
 * @author - Dmytro
 * @version - 1.0
 */
public class CommandContainer {

    private CommandContainer() {
    }

    private static final Logger LOGGER = Logger.getLogger(CommandContainer.class);

    private static Map<String, Command> commands = new HashMap<>();

    static {
        // common commands
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("viewProfile", new ViewProfileCommand());
        commands.put("editProfile", new EditProfileCommand());
        commands.put("noCommand", new NoCommand());
        commands.put("viewFaculty", new ViewFacultyCommand());
        commands.put("viewAllFaculties", new ViewAllFacultiesCommand());
        commands.put("confirmRegistration", new ConfirmRegistrationCommand());

        // client commands
        commands.put("client_registration", new ClientRegistrationCommand());
        commands.put("applyFaculty", new ApplyFacultyViewCommand());

        // admin commands
        commands.put("admin_registration", new AdminRegistrationCommand());
        commands.put("editFaculty", new EditFacultyCommand());
        commands.put("addFaculty", new AddFacultyCommand());
        commands.put("deleteFaculty", new DeleteFacultyCommand());
        commands.put("addSubject", new AddSubjectCommand());
        commands.put("editSubject", new EditSubjectCommand());
        commands.put("viewAllSubjects", new ViewAllSubjectsCommand());
        commands.put("viewSubject", new ViewSubjectCommand());
        commands.put("viewEnrollee", new ViewEnrolleeCommand());
        commands.put("createReport", new CreateFacultyReportCommand());
        commands.put("deleteSubject", new DeleteSubjectCommand());

    }

    /**
     * Returns command object with the given name and path of the resource.
     *
     * @param commandName
     *            Name of the command.
     * @return Command object.
     */
    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            LOGGER.trace("Command not found, with name - " + commandName);
            return commands.get("noCommand");
        }

        return commands.get(commandName);
    }
}



