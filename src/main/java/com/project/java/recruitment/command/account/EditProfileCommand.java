package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import com.project.java.recruitment.validation.AccountFormValidation;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Edit users profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class EditProfileCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(EditProfileCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOGGER.debug("Start executing edit profile command");

        String result = null;

        if (RequestType.GET == requestType) {
            result = doGet(request, response);
        } else if (RequestType.POST == requestType) {
            result = doPost(request, response);
        }

        LOGGER.debug("Finished executing edit profile command");
        return result;
    }

    /**
     * The page where the user can edit his profile.
     *
     * @return path to the edit profile page.
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {
        String result = null;
        HttpSession session = request.getSession(false);

        String userEmail = String.valueOf(session.getAttribute("user"));
        String role = String.valueOf(session.getAttribute("userRole"));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        UserDao userDao = daoFactory.getUserDao();
        User user = userDao.find(userEmail);

        request.setAttribute(Fields.USER_FULL_NAME, user.getFullName());
        LOGGER.trace("Set attribute 'full_name': " + user.getFullName());
        request.setAttribute(Fields.USER_EMAIL, user.getEmail());
        LOGGER.trace("Set attribute 'email': " + user.getEmail());
        request.setAttribute(Fields.USER_PASS, user.getPassword());
        LOGGER.trace("Set attribute 'password': " + user.getPassword());
        request.setAttribute(Fields.USER_LANG, user.getLang());
        LOGGER.trace("Set attribute 'lang': " + user.getLang());


        if ("client".equals(role)) {

            EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
            Enrollee enrollee = enrolleeDao.findEnrollee(user);

            request.setAttribute(Fields.ENROLLEE_CITY, enrollee.getCity());
            LOGGER.trace("Set attribute 'city': " + enrollee.getCity());
            request.setAttribute(Fields.ENROLLEE_REGION, enrollee.getRegion());
            LOGGER.trace("Set attribute 'region': " + enrollee.getRegion());
            request.setAttribute(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION, enrollee.getEducationalInstitution());
            LOGGER.trace("Set attribute 'educational_institution': " + enrollee.getEducationalInstitution());
            request.setAttribute(Fields.ENROLLEE_ATTACHMENT, enrollee.getAttachment());
            LOGGER.trace("Set attribute 'attachment': " + enrollee.getAttachment());
            request.setAttribute(Fields.ENROLLEE_IS_BLOCKED, enrollee.isBlocked());
            LOGGER.trace("Set attribute 'is_blocked': " + enrollee.isBlocked());

            result = Path.FORWARD_CLIENT_PROFILE_EDIT;
        } else if ("admin".equals(role)) {
            result = Path.FORWARD_ADMIN_PROFILE_EDIT;
        }
        return result;
    }

    /**
     * Invoked when user already edit his profile and wants to update it.
     *
     * @return path to the user profile if command succeeds, otherwise
     *         redisplays editing page.
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String oldUserEmail = request.getParameter("oldEmail");
        LOGGER.trace("Fetch request parameter: 'oldEmail' = " + oldUserEmail);


        String userFullName = request.getParameter(Fields.USER_FULL_NAME);
        LOGGER.trace("Fetch request parameter: 'full_name' = " + userFullName);
        String email = request.getParameter("email");
        LOGGER.trace("Fetch request parameter: 'email' = " + email);
        String password = request.getParameter("password");
        LOGGER.trace("Fetch request parameter: 'password' = " + password);
        String language = request.getParameter("lang");
        LOGGER.trace("Fetch request parameter: 'lang' = " + language);

        boolean valid = AccountFormValidation.validateUserParameters(userFullName, email, password, language);

        HttpSession session = request.getSession(false);
        String role = String.valueOf(session.getAttribute("userRole"));

        String result = null;

        if (valid) {
            if ("admin".equals(role)) {
                DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
                UserDao userDao = daoFactory.getUserDao();
                // should not be null !
                User user = userDao.find(oldUserEmail);

                LOGGER.trace("User found with email:" + user);

                user.setFullName(userFullName);
                user.setEmail(email);
                user.setPassword(password);
                user.setLang(language);

                LOGGER.trace("After calling setters with request parameters on user entity: "
                        + user);

                userDao.updateUser(user);

                LOGGER.trace("User info updated");

                // update session attributes if user changed it
                session.setAttribute("user", email);
                session.setAttribute(Fields.USER_LANG, language);

                result = Path.REDIRECT_TO_PROFILE;

            } else if ("client".equals(role)) {
                // if user role is client then we should also update enrollee
                // record
                // for him
                String educationalInstitution = request.getParameter(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION);
                LOGGER.trace("Fetch request parameter: 'educational_institution' = " + educationalInstitution);
                String region = request.getParameter(Fields.ENROLLEE_REGION);
                LOGGER.trace("Fetch request parameter: 'region' = " + region);
                String city = request.getParameter(Fields.ENROLLEE_CITY);
                LOGGER.trace("Fetch request parameter: 'city' = " + city);
                boolean blockedStatus = Boolean.parseBoolean(request
                        .getParameter(Fields.ENROLLEE_IS_BLOCKED));
                LOGGER.trace("Fetch request parameter: 'isBlocked' = " + blockedStatus);

                valid = AccountFormValidation.validateEnrolleeParameters(city,
                        region, educationalInstitution);
                if (valid) {
                    DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
                    UserDao userDao = daoFactory.getUserDao();
                    // should not be null !
                    User user = userDao.find(oldUserEmail);

                    LOGGER.trace("User found with email:" + user);

                    user.setFullName(userFullName);
                    user.setEmail(email);
                    user.setPassword(password);
                    user.setLang(language);

                    LOGGER.trace("After calling setters with request parameters on user entity: "
                            + user);

                    userDao.updateUser(user);

                    LOGGER.trace("User info updated");

                    EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();

                    // should not be null !!
                    Enrollee enrollee = enrolleeDao.findEnrollee(user);

                    enrollee.setCity(city);
                    enrollee.setRegion(region);
                    enrollee.setEducationalInstitution(educationalInstitution);
                    enrollee.setBlocked(blockedStatus);

                    LOGGER.trace("After calling setters with request parameters on enrollee entity: "
                            + enrollee);

                    enrolleeDao.updateEnrollee(enrollee);
                    LOGGER.trace("Enrollee info updated");

                    // update session attributes if user changed it
                    session.setAttribute("user", email);
                    session.setAttribute(Fields.USER_LANG, language);

                    result = Path.REDIRECT_TO_PROFILE;
                } else {
                    request.setAttribute("errorMessage", "Please fill all fields properly!");
                    LOGGER.error("errorMessage: Not all fields are properly filled");
                    result = Path.REDIRECT_EDIT_PROFILE;
                }
            }
        } else {
            request.setAttribute("errorMessage", "Please fill all fields properly!");
            LOGGER.error("errorMessage: Not all fields are properly filled");
            result = Path.REDIRECT_EDIT_PROFILE;
        }
            return result;
    }
}
