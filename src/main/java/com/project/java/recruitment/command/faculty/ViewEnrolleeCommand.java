package com.project.java.recruitment.command.faculty;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.EnrolleeDao;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.Enrollee;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.Fields;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * View enrollee's profile command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class ViewEnrolleeCommand extends Command {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(ViewEnrolleeCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {
        LOG.debug("Start executing view enrollee command");

        String result = null;

        if (requestType == RequestType.GET) {
            result = doGet(request, response);
        } else if (requestType == RequestType.POST) {
            result = doPost(request, response);
        }

        LOG.debug("Finished executing view enrollee command");

        return result;
    }

    /**
     * Forwards admin to enrollee profile.
     *
     * @return path to enrollee profile page
     */
    private String doGet(HttpServletRequest request, HttpServletResponse response) {

        int userId = Integer.parseInt(request.getParameter("userId"));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);

        UserDao userDao = daoFactory.getUserDao();
        // should not be null !
        User user = userDao.find(userId);

        request.setAttribute("full_name", user.getFullName());
        LOG.trace("Set the request attribute: 'full_name' = " + user.getFullName());
        request.setAttribute("email", user.getEmail());
        LOG.trace("Set the request attribute: 'email' = " + user.getEmail());
        request.setAttribute("role", user.getRole());
        LOG.trace("Set the request attribute: 'role' = " + user.getRole());

        EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();
        // should not be null !!
        Enrollee enrollee = enrolleeDao.findEnrollee(user);

        request.setAttribute(Fields.ID, enrollee.getId());
        LOG.trace("Set the request attribute: 'id' = " + enrollee.getId());
        request.setAttribute(Fields.ENROLLEE_CITY, enrollee.getCity());
        LOG.trace("Set the request attribute: 'city' = " + enrollee.getCity());
        request.setAttribute(Fields.ENROLLEE_REGION, enrollee.getRegion());
        LOG.trace("Set the request attribute: 'region' = " + enrollee.getRegion());
        request.setAttribute(Fields.ENROLLEE_EDUCATIONAL_INSTITUTION, enrollee.getEducationalInstitution());
        LOG.trace("Set the request attribute: 'educational_institution' = "
                + enrollee.getEducationalInstitution());

        request.setAttribute(Fields.ENROLLEE_IS_BLOCKED, enrollee.isBlocked());
        LOG.trace("Set the request attribute: 'isBlocked' = " + enrollee.isBlocked());

        return Path.FORWARD_ENROLLEE_PROFILE;
    }

    /**
     * Changes blocked status of enrollee after submitting button in enrollee view.
     *
     * @return redirects to view enrollee page
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        int enrolleeId = Integer.parseInt(request.getParameter(Fields.ID));

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        EnrolleeDao enrolleeDao = daoFactory.getEnrolleeDao();

        Enrollee enrollee = enrolleeDao.findEnrollee(enrolleeId);

        boolean updatedBlockedStatus = !enrollee.isBlocked();
        enrollee.setBlocked(updatedBlockedStatus);

        LOG.trace("Enrollee with 'id' = " + enrolleeId
                + "and changed 'isBlocked' status = " + updatedBlockedStatus
                + " record will be updated.");

        enrolleeDao.updateEnrollee(enrollee);

        return Path.REDIRECT_ENROLLEE_PROFILE + enrollee.getUserId();
    }
}
