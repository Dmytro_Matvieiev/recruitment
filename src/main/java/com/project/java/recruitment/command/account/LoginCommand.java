package com.project.java.recruitment.command.account;

import com.project.java.recruitment.Path;
import com.project.java.recruitment.command.Command;
import com.project.java.recruitment.dao.UserDao;
import com.project.java.recruitment.dao.factory.DaoFactory;
import com.project.java.recruitment.dao.factory.FactoryType;
import com.project.java.recruitment.domain.User;
import com.project.java.recruitment.utils.RequestType;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Login command.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class LoginCommand extends Command {

    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, RequestType requestType)
            throws IOException, ServletException {

        LOGGER.debug("Start executing login command");

        String result = null;

        if (requestType == requestType.POST) {
            result = doPost(request, response);
        } else {
            result = null;
        }

        LOGGER.debug("Finished executing login command");
        return result;
    }

    /**
     * Login user in portal.
     * After successfully login user redirect to view all faculties.
     * @param request
     * @param response
     * @return
     */
    private String doPost(HttpServletRequest request, HttpServletResponse response) {
        String result = null;

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        DaoFactory daoFactory = DaoFactory.getFactoryByName(FactoryType.MYSQL_DAO_FACTORY);
        UserDao userDao = daoFactory.getUserDao();
        User user = userDao.find(email, password);

        LOGGER.trace("User found: " + user);
        if (user == null) {
            request.setAttribute("errorMessage", "Can't find user with this login/password");
            LOGGER.error("errorMessage: Can't find user with login - " + email + " and password - " + password);
            result = null;
        } else if (!user.isActive()) {
            request.setAttribute("errorMessage", "You are not registered!");
            LOGGER.error("errorMessage: User is not registered or did not complete his registration.");
            result = null;
        } else {
            HttpSession session = request.getSession(true);
            LOGGER.trace("Set the session attribute 'user' = " + user.getEmail());
            session.setAttribute("user", user.getEmail());

            LOGGER.trace("Set the session attribute: 'userRole' = " + user.getRole());
            session.setAttribute("userRole", user.getRole());

            LOGGER.trace("Set the session attribute 'lang' = " + user.getLang());
            session.setAttribute("lang", user.getLang());

            LOGGER.info("User: " + user + " logged as " + user.getRole());

            result = Path.REDIRECT_TO_VIEW_ALL_FACULTIES;
        }
        return result;
    }
}
