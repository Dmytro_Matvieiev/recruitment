package com.project.java.recruitment.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * POJO for entity Enrollee.
 * Every enrollee has city, region,
 * educational institution, attachment with diploma,
 * active status and role in system.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class Enrollee extends Entity {
    private int userId;
    private String city;
    private String region;
    private String educationalInstitution;
    private byte[] attachment;
    private boolean isBlocked;

    public Enrollee() {
    }

    public Enrollee(String city, String region,
                    String educationalInstitution, byte[] attachment, User user) {
        this(city, region, educationalInstitution, attachment, user.getId());
    }

    public Enrollee(String city, String region,
                    String educationalInstitution, byte[] attachment, int userId) {
        this.userId = userId;
        this.city = city;
        this.region = region;
        this.educationalInstitution = educationalInstitution;
        this.attachment = attachment;
        this.isBlocked = false;
    }

    @Override
    public String toString() {
        return "Enrollee{" +
                "userId=" + userId +
                ", city='" + city + '\'' +
                ", region='" + region + '\'' +
                ", educationalInstitution='" + educationalInstitution + '\'' +
                ", attachment=" + Arrays.toString(attachment) +
                ", isBlocked=" + isBlocked +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enrollee enrollee = (Enrollee) o;
        return userId == enrollee.userId &&
                Objects.equals(city, enrollee.city) &&
                Objects.equals(region, enrollee.region) &&
                Objects.equals(educationalInstitution, enrollee.educationalInstitution);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, city, region, educationalInstitution);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEducationalInstitution() {
        return educationalInstitution;
    }

    public void setEducationalInstitution(String educationalInstitution) {
        this.educationalInstitution = educationalInstitution;
    }

    public byte[] getAttachment() {
        return attachment;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
