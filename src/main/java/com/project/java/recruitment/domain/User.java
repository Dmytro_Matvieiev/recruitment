package com.project.java.recruitment.domain;

import java.util.Objects;

/**
 * POJO for entity User.
 * Every user has email, password, first name, last name,
 * active status and role in system.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class User extends Entity {
    private String email;
    private String password;
    private String fullName;
    private String role;
    private String lang;
    private boolean isActive;

    public User() {
    }

    public User(String email, String password, String fullName,
                Role role, String lang, boolean isActive) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.role = role.getName();
        this.lang = lang;
        this.isActive = true;
    }

    @Override
    public String toString() {
        return "User{" +
                " email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", role='" + role + '\'' +
                ", lang='" + lang + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(fullName, user.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password, fullName);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
