package com.project.java.recruitment.domain;

import java.util.Objects;

/**
 * POJO for entity Faculty.
 * Every faculty has name, number of departments, total places
 * and budget places.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class Faculty extends Entity {
    private String nameRu;
    private String nameEng;
    private int totalPlaces;
    private int budgetPlaces;

    public Faculty() {
    }

    public Faculty(String nameRu, String nameEng, int totalPlaces, int budgetPlaces) {
        this.nameRu = nameRu;
        this.nameEng = nameEng;
        this.totalPlaces = totalPlaces;
        this.budgetPlaces = budgetPlaces;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public int getTotalPlaces() {
        return totalPlaces;
    }

    public void setTotalPlaces(int totalPlaces) {
        this.totalPlaces = totalPlaces;
    }

    public int getBudgetPlaces() {
        return budgetPlaces;
    }

    public void setBudgetPlaces(int budgetPlaces) {
        this.budgetPlaces = budgetPlaces;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "nameRu='" + nameRu + '\'' +
                ", nameEng='" + nameEng + '\'' +
                ", totalPlaces=" + totalPlaces +
                ", budgetPlaces=" + budgetPlaces +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return totalPlaces == faculty.totalPlaces &&
                budgetPlaces == faculty.budgetPlaces &&
                nameRu.equals(faculty.nameRu) &&
                nameEng.equals(faculty.nameEng);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameRu, nameEng, totalPlaces, budgetPlaces);
    }
}
