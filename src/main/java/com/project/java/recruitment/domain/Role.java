package com.project.java.recruitment.domain;

/**
 * User's roles.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public enum Role {
    ADMIN, CLIENT;

    public String getName() {
        return name().toLowerCase();
    }
}
