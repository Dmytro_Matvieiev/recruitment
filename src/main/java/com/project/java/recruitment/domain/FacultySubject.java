package com.project.java.recruitment.domain;

import java.util.Objects;

/**
 * POJO for faculty subject.
 * Every POJO has faculty id and subject id (one to many).
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class FacultySubject extends Entity {
    private int subjectId;
    private int facultyId;

    public FacultySubject() {
    }

    public FacultySubject(int subjectId, int facultyId) {
        this.subjectId = subjectId;
        this.facultyId = facultyId;
    }

    @Override
    public String toString() {
        return "FacultySubject{" +
                "subjectId=" + subjectId +
                ", facultyId=" + facultyId +
                '}';
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(int facultyId) {
        this.facultyId = facultyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FacultySubject that = (FacultySubject) o;
        return subjectId == that.subjectId &&
                facultyId == that.facultyId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjectId, facultyId);
    }
}
