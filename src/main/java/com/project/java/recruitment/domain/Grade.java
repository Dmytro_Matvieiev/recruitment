package com.project.java.recruitment.domain;

/**
 * POJO for entity Faculty.
 * Every faculty has name, number of departments, total places
 * and budget places.
 *
 * @author - Dmytro Matvieiev
 * @version - 1.0
 */
public class Grade extends Entity {

    private int enrolleeId;
    private int subjectId;
    private int value;
    private String examType;

    public Grade() {
    }

    public Grade(int subjectId, int enrolleeId, int value, String examType) {
        this.subjectId = subjectId;
        this.enrolleeId = enrolleeId;
        this.value = value;
        this.examType = examType;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "subjectId=" + subjectId +
                ", enrolleeId=" + enrolleeId +
                ", value=" + value +
                ", examType='" + examType + '\'' +
                '}';
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getEnrolleeId() {
        return enrolleeId;
    }

    public void setEnrolleeId(int enrolleeId) {
        this.enrolleeId = enrolleeId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }
}
